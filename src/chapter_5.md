# Pop the hood

**TLDR;**

As the reference bare metal Rust appliacation is now established we can briefly look at ARM Cortex-M4 architecture - especially ALU. Next topic will be booting and initialization. We'll write a `build.rs` script, `.gdbinit` file and (cargo) `Makefile`. We finish by writing code that will enable us *Semihosting* - print messages from target in console.

## RISC

ARM processors and microcontrollers are founded on the *Reduced Instruction Set* (RISC). The instruction set has a fixed length and aims to finish the instruction in one clock cycle - though some instructions might take more clock cycles (e.g. division). Keeping instructions simple means reducing the complexity of the processor and putting the complexity on the shoulders of the compiler.

Another aspect is *Load Store Architecture*. The processor/microcontroller can operate only on variables that are *loaded* from the memory into the registers, performing operation/s and *storing* the computed value back from the register to the memory. This means that the source and destination encoded in the instructions operates on the index of the register rather than an address of the value in memory - reducing the number of bits in the instruction itself to encode them. 

## Pipeline

Pipelining in the processor allows to achieve certain degree of parallelism. Therefore we can process other instructions while the previous one goes through different stage, thereby allowing the processor to deliver a higher throughput. Each stage thereby performs a smaller volume of the processing per cycle.

![pipeline](../img/pipeline.png)

Figure 1: Shows how the instructions are executed in the pipeline of the processor. Source: [wikipedia]

The *Fetch* stage fetches the instruction from memory. The following *Decode* stage decodes the instruction  and prepares data path control signals for the next cycle. During *Execute*, the operands are read from register banks, shifted, combined in the ALU and the result written back in the last stage - *Write-back*.

> The processor fetches, decodes and executes instruction at the same time.

> Keep in mind that the PC points to the instruction that is currently being fetched. The address of instruction being currently executed on pipeline consisting of 3 stages would be `PC - 4`.

The number of pipelines varies - there can be as low as 2 (*ARM Cortex-M0*) up to 31 (*Intel [Netburst]*).

## General purpose registers and stack

The *Cortex-M4* processor has registers `R0` through `R15`. `R0-R12` are 32-bit general purpose registers for data operations. There are some 16-bit Thumb instruction that can access only the low registers `R0-R7`. 

There are two stack pointers - *Main Stack Pointer* (MSP) and *Process Stack Pointer* (PSP). MSP is the default stack pointer which is used by the operating system and expections handlers. PSP is used by user application. The lowest 2 bits are always 0 meaning words are always aligned.

The most common (and default) implementaion in *ARM* is fully descending, which means that the stack grows towards lower address and the stack pointer points to the last item pushed on the stack.

`R14` is used in order to save the address when subroutine is called.

`R15` holds the *current program counter + 4* as the core contains pipline which fetches, decodes and executes the instruction.

## Special Registers

Beside general purpose registers the *Cortex-M4* has the following special registers:

* *Program status registers* (PSRs) - Provide arithmetic and logic processing flags (zero flag and carry flag), execution status, and current executing interrupt number
* *Interrupt Mask register*
  * PRIMASK - Disable all interrupts except the nonmaskable interrupt (NMI) and hard fault
  * FAULTMASK - Disable all interrupts except the NMI
  * BASEPRI - Disable all interrupts of specific priority level or lower priority level
* *Control register* (CONTROL) - Define privileged status and stack pointer selection

These registers can be accessed only by special instructions.

## Operation Modes

The *Cortex-M4* processor has two modes and two privilege levels. The operation modes (thread mode and handler mode) determine whether the processor is running a normal program or running an exception handler like an interrupt handler or system exception handler (see Figure 2.4). The privilege levels (privileged level and user level) provide a mechanism for safeguarding memory accesses to critical regions as well as providing a basic security model.

The running program is executed in thread mode and can be either in a privileged state
or a user state, whereas exception handlers can only be in a privileged state.

Privileged state provides program full access to all memory ranges (except when prohibited by *MPU* settings) and can use all supported instructions.

When a user program wants to perform some privileged operation it has to invoke an exception handler which results in context switch. There is a dedicated interrupt controller called *Nested vectored interrupt controller* (NVIC) that notifies the core about pending interrupt. We'll cover *NVIC* and interrupts in later chapters.

## Instruction sets
Let's list all the *Cortex-M* families and their respective architectures:

* **ARMv6-M**: *Cortex-M0/M0+*
* **ARMv7-M**: *Cortex-M3*
* **ARMv7E-M**: *Cortex-M4/M7*

The key point is that the architectures are binary instruction upward compatible from *ARMv6-M* to *ARMv7-M* to *ARMv7E-M*. Binary instructions available for the *Cortex-M0/M0+* can execute without modification on the *Cortex-M3/M4/M7*. 

Only *Thumb* and *Thumb-2* instruction sets are supported in *Cortex-M* architectures. The legacy 32-bit *ARM* instruction set isn't supported.

Here we have summary of the instructions sets (I kept *ARM* for comparison):

### ARM

- The *ARM* instructions are all 32 bits in size, and have a 3 operand instruction set format (operands can be registers, memory allocation or literal data).
- Most instructions are conditional (some exceptions include CLREX, PLD, RFE, BKPT, DMB, BLX).
- Access to 16 registers (`R0-R15`)
- Barrel shifter is available as part of the instructions
- Processor “wakes up” in this state and all interrupts are handled in ARM state
- Provides high performance code at the expense of greater code size

### Thumb

- Thumb instructions are 16 bit in size with the exception of BL/BLX (which are 32 bit)
- 2 operand instruction set format
- Only branch instructions are conditional
- Access to 8 visible registers with the exception of a few insructions which can access the other registers
- Stack accessible only through PUSH and POP instructions (which work on a fully descending stack)
- Barrel shifter not available as part of the instruction set; any shifts must be done through separate instructions
- Under the hood, the Thumb instruction is “expanded” into an *ARM* instruction
- Provide better code density at the expense of performance, although sometimes *Thumb* state can improve perfomance (e.g. the code might fit better in the cache)

### Thumb-2

- Combination of 16 bit and 32 bit instructions, with the aim to deliver *ARM* performance code with *Thumb* style code density
- Conditional executions are supported by the use of *If-Then (IT)* blocks, which means up to 4 instructions can be executed conditionally.
- Full access to VFP instructions
- It is possible to have a *Thumb-2* only processor (e.g. Cortex-M3)

*Thumb-2* is a much newer instruction set which was first introduced as part of *ARMv6-T2* and made the default from *ARMv7-M* onwards. The instruction set has been vastly expanded incorporating DSP as well as FPv4-SP floating point extensions.

![Instruction set](../img/instruction_set.png)
Figure 2: Sets of instruction for *ARM Cortex-M* processors. Source: [Instruction set source]

> In architectures supporting *Thumb* and *ARM* states tou'll find in the linker sections -`.glue_7` and `.glue_7t`. These allow ARM instructions to call Thumb instructions.

## Vector table

According to *ARM Cortex-M4* specification the address `0x00000000` contains so called *Vector table*. The *Vector table* is an array of addresses. The first address belongs to the address of Stack pointer. The next 16 items are standardized by *ARM*. The rest are defined by the vendor.

![Vector table](../img/tex/vector_table.png)

Figure 3: Vector table with listed handlers/interrupts.

## Boot procedure

Upon the reset the *Cortex-M4* loads the stack pointer from the Vector table located at address `0x00000000` to stack pointer register. Then it fetches the first instruction from the address pointed by reset handler.

The memory layout will be specified in the next post. However, note that vendors are free to superseed the boot procedure sequence with their prorietary thing.

The *STM32* has the FLASH memory at `0x08000000`. So how does it boot? Well, there are special pins on the chip - *boot pins* - as well as boot bits in special part of FLASH memory - *option bytes* - that decide what memory will be mapped to `0x00000000`. The advantage of this approach is that you can select whether to boot from FLASH or RAM.

**TODO: SYSCFG_MEMRMP**

## Initialize Rust environment

So the processor is booted. What shall we do now? Execute *Rust* code? No, at least not yet! Theoretically we could if we use only local variables but want to get rid of this restriction. 

In the linker file we edited 2 memory regions: *BSS* and *DATA*.

The BSS section specifies an area of RAM memory that *Rust* will expect to be zeroed out. This section is truncated in the resulted binary as it would consist only of zeros resulting in larger executable.

The DATA section specifies an area of RAM memory that *Rust* will expect initialized values. These data need to be copied to from FLASH to RAM.

We could do this in a simple loop but Jorge Aparicio already prepared such functionality in crate called `r0`. Therefore we add the crate as dependency into our *Cargo.toml* file:

```toml
[dependencies]
r0 = "0.2.2"
```

We'll need to notify *Rust* that we'll use this crate by putting `extern crate r0;` to the top of `the main.rs`.
In the `reset_handler` we call the `zero_bss` & `init_data` functions. Also note the extern "C" block where we declare the variables we defined in the linker script earlier.

```rust
// main.rs

//! This is the documentation for `cortex`
//! This crate contains the initialization code for the Cortex-M4 microcontroller
#![no_std]
#![no_main]
extern crate r0;
...

/// Initializes the Rust enviroment
/// 
/// 
/// # Remarks
///
/// This is the first function that is called after the processor is released from reset. The /// Rust environment is not properly intialized until we call r0::zero_bss and r0::init_data 
/// functions.
///
/// #Safety
///
/// This function must never return otherwise it will cause undefined behaviour.
#[no_mangle]
pub extern "C" fn reset_handler() -> ! {
// The type, `u32`, indicates that the memory is 4-byte aligned
    extern "C" {
        static mut _sbss: u32;
        static mut _ebss: u32;

        static mut _sdata: u32;
        static mut _edata: u32;

        static _sidata: u32;
    }

    unsafe {
        // Zero the BSS section in RAM
        r0::zero_bss(&mut _sbss, &mut _ebss);
        // Copy variables in DATA section in FLASH to RAM
        r0::init_data(&mut _sdata, &mut _edata, &_sidata);
    }

    extern "Rust" {
        fn main () -> !;
    }

    unsafe { main(); }
  
    loop {}
}
```

> Extern block is used to declare functions or variables defined in some other library that the final Rust executable will be linked with. Declared functions are considered `unsafe`.

> The extern block can be annotated by attribute `#[link(name = "NAME")]`, where `NAME` is the name of the library

As a last thing we make call to `main` function but define it as external so we can use this crate as a lib in the next post and just focus on the application. Therefore we need to change the signature of `main` function and add attribute `no_mangle`.

> I leave the `loop {}` though the signature of `main` function requires not to return. I take it as a precaution and later we will implement proper error handling.

**TODO: Proper error handling**

**TODO: *unmangled* name. Downside: types signatures are lost. How to recover them? traits? Related: weak symbols for e.g. exceptions.**

**TODO:Guaranteed `panic!`-free programs with this one weird trick!**

Its good practice to document your code. *Rust* supports 3 types of comments:

* The `//` is treated just as regular comment.

* The `///` is treated as documentation supporting Markdown syntax for the following item.

* The `!//` is treated as documentation supporting Markdown syntax for the module (file).

>There is also /* */ type of comment but it has no meaning for documentation.

> The `///` is treated as `#[doc]` attribute for the following item. Likewise `//!` are treated as`#![doc]` for the crate.

To generate the documentation you simply run:

```shell
$ cargo doc --open
```

This will render the documentation of your crate in your default web browser as HTML.

![crate documentation](../img/crate_doc.png)

Figure 4: Web page with documentation about the crate.

In crate we can click on `reset_handler` under *Functions* and inspect the documentation:

![function documentation](../img/function_doc.png)

Figure 5: Web page with documentation about the `reset_handler`.

## Const vs static

```rust
const RUST_CONST_ZERO: u32 = 0; // not allocated
const RUST_CONST: u32 = 0xABCD; // not allocated
static RUST_STATIC_ZERO: u32 = 0; // .text
static RUST_STATIC: u32 = 0xDEAD; // .text
static mut RUST_STATIC_MUT_ZERO: u32 = 0; // .bss
static mut RUST_STATIC_MUT: u32 = 0xBEEF; // .data
```

In the above code excerpt you can see three types of declaring globals in *Rust*. The global declarations are divided into:

* *Constants* 
* *Statics*

A *constant* declaration - `RUST_CONST_ZERO` and `RUST_CONST` - represents a value. There is no need to allocate memory for globals declared as const, as the values can be directly inserted where they are used by the compiler.

The *static* globals are immutable by default, but can be made mutable by the `mut`keyword. As variables `RUST_STATIC_ZERO` and `RUST_STATIC` are immutable, they are allocated in the read-only section called `.text`. On the other hand the last two variables are marked with `static mut`. `RUST_STATIC_MUT_ZERO` variable is assigned to the `.bss` section and on startup (reset handler) the value is initialized to zero.`RUST_STATIC_MUT` has a non-zero value that has to be stored in FLASH memory prior to execution and on startup copied to RAM.

[]: 