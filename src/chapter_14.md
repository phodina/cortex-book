# A moment please!

**TLDR;**

## Interrupts and handlers

The most important fact to know is that *ARM Cortex-M* uses the “reversed” priority numbering
scheme for interrupts. Therefore priority zero corresponds to the **highest urgency** interrupt and higher numerical values of priority correspond to **lower urgency**.

**TODO: Explain differences**

**TODO: Scheduling strategies: Super loop, cooperative, preemptive.**

**TODO:Interrupts: asm!("cpsid i"), asm!("cpsie i"), data races and atomic operations.**

**TODO: User interrupts** http://libopencm3.org/docs/latest/stm32f4/html/group__CM3__nvic__defines__STM32F4.html

**TODO: https://www.embedded.fm/blog/2016/11/13/discovery-button-interrupts**

**TODO: Reset handler not really interrupt as we are in thread mode??**

## Cortex-M handlers

What is the difference between interrupts and handlers?

When an exception/interrupt takes place, a number of things happen:

- Stacking

> Pushing the `r0-r7` to stack. If the interrupted code used PSP as stack pointer it will be used for the handler otherwise MSP will be used.

- Vector fetching

> Read the first instruction of the exception/interrupt handler from the address of the corresponding handler in the vector table.

- Update of the stack pointer(SP), link register(LR), program counter(PC) na d program state register (PSR).

Upon exception/interrupt handler exit the processor will restore the interrupted state.

- Unstacking

> The pushed registers to the stack will be restored.

- NVIC register update

> The active bit of the exception will be cleared.

When entering an exception handler, the LR is updated to a special value called EXC_RETURN, with the upper 28 bits all set to 1. When loaded into the PC at the end of the exception handler execution, will cause the processor to perform  an exception return sequence. The EXC_RETURN value has bit [31:4] all set to 1, and  bit[3:0] provides information required by the exception return operation. The LR value is updated automatically when entering the exception handler.

- LR = 0XFFFFFFF1

> Return to handler mode (nested exception/interrupt)

- LR = 0XFFFFFFF9

> Return to Thread mode and on return use the main stack

- LR = 0XFFFFFFFD

> Return to THread mode and on return use the process stack

## Interrupt tail-chaining

When an exception takes place but the micro-controller is handling another exception of the same or higher priority the exception will be pended. The unstacking and stacking between interrupts is skipped therefore reducing the timing gap between two exception handlers.
![Tail chaining](../img/tail_chaining.jpg)
Figure 1: Souce: [Tail chaining source]

## Interrupt late-arrival

When an exception takes place and the processor has started the stacking process, and if during this delay a new exception arrives with higher preemption priority, the late arrival exception will be processed first.
![Late arrival](../img/late_arrival.jpg)

Figure 2: Source [Late arrival source]

## Exception handlers

**TODO: Change into table**

- Reset Handler

> Reset

- NMI Handler

> Nonmaskable handler (external NMI input)

- Hard Fault Handler

> All fault conditions, if the corresponding fault handler is not enabled

- Memory Management Handler

> Memory management fault - MPU violation or access to illegal locations

- Bus Fault

> Bus error - occurs when AHB interface receives an error response from a bus slave (prefetch or data abort)

- Usage Fault

> Exceptions fue to program error or trying to access coprocessor

- SVCall Handler

> Triggered by the SVC instruction. Used by the RTOS to start the scheduler.

- Debug Monitor

> An alternative debug mode. Breakpoints and watchpoints trigger this exception. Can be used e.g. to implement dubgger stub, to debug only dedicated process or for unit testing.

- PendSV Handler

> An interrupt request used by the OS to force a context switch if no other interrupt is active.

- SysTick Handler

> Used as a time base in RTOS

## Revise Vector table

As of now the Vector table is implemented as simple array holding the address of the functions for the appropriate vector. Though convenient it isn't the best API.

**TODO: Reimplement as Union with a default handler**

```rust
// main.rs
extern "C" fn hard_fault_handler()
{
  unsafe {asm!("BKPT #01");}
  loop{}
}

#[link_section = ".vector_table.exceptions"]
#[no_mangle]
pub static __EXCEPTIONS: [extern "C" fn(); 14] = [
    hard_fault_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    default_handler,
];
```

>

**TODO: Cfg on ARM architecture**

## Handler as closure

### Handling interrupts with Closures

When we implemented interrupt for the *GPIO* button we used global variables - especially mutable - and unsafe block. This approach does not scale well.

Is there a way to tackle interrupts using a more idiomatic Rust code? The answer is to ownership rules which apply to closures.  

```rust
// Declaring a Pushbutton Type globally
static mut PUSHBUTTON = 0;

fn main() {
let gpio = gpio();
// Using PUSHBUTTON requires unsafe
// e.g.: unsafe { PUSHBUTTON }
loop { /* ... */ }
}

pub extern fn GPIO1_IRQHandler() {
let gpio = gpio();
let pushed = gpio.input();
// Writing to the variable is considered unsafe
unsafe { PUSHBUTTON = pushed; }
}
```

> The interrupt handler is in the global scope, so it can only access global variables and therefore the pushbutton must be declared as static mut. This require all read and writes to the buffer to be handled within unsafe blocks.

So let's refactor the code with interrupts implemented as *closures*.

```rust
fn main() {
 let mut gpio = gpio();
 let pushbutton = PushButton::new();
 let mut bt = pushbutton.in();
 // Register a closure on the GPIO. The closure will be called each
 // time a new pushbutton state generated. The
 // `move' keyword is used to move ownership of the `bt' variable.
 gpio.on_down(move |pushed| bt.send(pushed));

 // Reading from buffer is safe
 // e.g.: &buffer[..];
 loop { /* ... */ }
 }
```

We replace the global state with a pushbutton variable that is owned by the main function stack frame. The frame will never get deallocated as the main function contains an infinite loop or until we get a fatal error.
Therefore we can rely on variables owned by the main stackframe to live for the duration of the application - the lifetime of `pushbutton` is for practical purpuse the same as of `PUSHBUTTON`. Though the variable is not `static mut` and let's the programmer avoid `unsafe` blocks - letting compiler to ensure safety.
The most interesting part is the `pushbutton`, Based based on the same type as the Rust standard library `channel` type which provides a facility for interprocess communication. The channel has one *read end*
and one *write end* enabling the producing process to send a stream of messages to the consuming process.

> This is required for the GPIO callback to be able to write to the pushbutton while the main function retains ownership of the pushbutton - write end.

The closure is implemented by the line `gpio.on_single(move |pushed| bt.send(pushed));`. It takes ownership of the *write end* for the `pushbutton`. The closure is passed as an argument to the `on_down` method of the GPIO ensuring that the closure is called each time a pushbutton is pressed with the `state` as an argument.

Now we need to ensure also that the `GPIO_Interrupt` calls the closure created in `main` function. The interrupt handler can only access global variables - so we store the closure in static variable. As closures still don't have static initializer functions we'll need to use raw pointers - or Option to avoid null pointer.

```rust
static mut CLOSURE: Option<*const Fn()> = None;
```

> The CLOSURE variable is unsafe because it's static mutable variable.

Now we need to implement register and dispatch events for this handle. We'll need an unsafe block where the responsibility of the ownership of the closure is transffered from the compiler to the programmer - through functions `from_raw` and `into_raw` (they start and stop the borrow checker on the variable).

```rust
// Registers an interrupt handler
fn register(f: Box<Fn()>) {
 unsafe {
      // Deallocate old handler if existing
      if let Some(old) = CLOSURE {
      // Remove the global reference
      CLOSURE = None;
      // Return the ownership of the pointer to a managed Box.
      // This transfers the responsibility of deallocating the
      // closure back to the compiler.
      let _ = boxed::Box::from_raw(old);
      // Omitting the above line will not trigger compilation
      // error, but the old closure value will be leaked.
   		}
   	// Consume the Box pointer and return a raw pointer to the
   	// closure. This transfers the responsibility of deallocating
   	// the closure from the compiler to the programmer
   	let raw = boxed::into_raw(f);
   	// Save the closure pointer in the global reference
   	CLOSURE = Some(raw);
  	}
 }

 // Dispatch an event by calling the closure if it is registered
 fn dispatch() {
 	// The closure is stored in a global mutable variable,
 	// so the access to this variable is unsafe
 	unsafe {
 	// Unwrap the closure value
 	if let Some(func) = CLOSURE {
 		// The closure is stored behind a pointer which must
 		// be dereferenced, it is called with its environment
 		// by invoking the `call' function
 		(*func).call(())
 		}
 	}
 }
```

And the GPIO implementation:

```rust
impl GPIO {
	pub fn on_down(&'static self, callback: Box<Fn(u32)>) {
 // Make sure the callback for a single conversion is enabled
 self.int_enable(IEN_SINGLE);
 // Call the utility to register an interrupt handler.
 // The `move' keyword is used to move ownership of
 // the callback function into the closure
 register(Box::new(move || {
 // Clear the interrupt signal to mark it as handled
 self.int_clear(IF_SINGLE);
 // Call the interrupt handler with the ADC sample
 callback(self.data_single_get())
 }));
 }
 // Clears interrupt signals
pub fn int_clear(&self, flag: u32) { /*...*/ }
// Get current state on pin
pub fn get_in(&self) -> u32 { /*...*/ }
}

fn GPIO_IRQHandler() {
dispatch();
}
```

## Reallocate Vector table

Let's say we don't want to flash our executable to our board through the JTAG or SWD interface. What would be the reason? Probably the application will be deployed in the field and the customer will require update of the running firmware through some nice interface - USB, Ethernet or WiFi (realized over serial communication with wireless chip). This means we need some sort of bootloader implemented in the firmware. We'll probably want the application to handle exceptions and interrupts in different way then the bootloader would (e.g. Hard fault). That means the vector tables should be independent. How to achieve this?

Well, there are two ways. One solution let the items in the vector table point to the RAM. This way we will just change the functions at the specific addresses. However this approach is cumbersome as we don't know anything about the new handler.

So why do I mention it? Well, this is the only choice on Cortex-M0. The rest of the Cortex-M3 cores (including Cortex-M0+) have a special register called `Vector Table Offset Register` (VTOR) allowing to specify the address of the vector table. Therefore all the bootloader has to do is to setup the VTOR, then the stack pointer according to the first item in the new vector table (this is normally handled by   hardware) and then jump to reset handler. This will then pass the from control bootloader to the application.

```rust
// main.rs
const SCB_BASE: u32 = 0xE000ED00;
const SCB_VTOR: *mut u32 = (SCB_BASE + 0x08) as *mut u32;

pub fn remap_vector_table(offset: u32) {
    unsafe{core::ptr::write_volatile(SCB_VTOR, offset & 1<<8)};
}
```

## NVIC

### Priority bits

The number of priority levels in the *ARM Cortex-M* core is configurable - depends on vendor implementation. However, there is a minimum number of interrupt priority bits that need to be implemented,  which is 2 bits in *ARM Cortex-M0/M0+* and 3 bits in *ARM Cortex-M3/M4*. The priority bits are implemented in the **most-significant** bits of the priority configuration registers in the NVIC.

The interrupt priorities don’t need to be uniquely assigned - it is legal to assign the same interrupt priority to multiple interrupts.

> After reset all interrupts and exceptions with configurable priority have the same default priority of zero - meaning the highest possible interrupt urgency.

The interrupt priority registers for each interrupt is further divided into two parts. 

- preempt priority
- subpriority

The number of bits in each part of the priority registers is configurable via the *Application Interrupt and Reset Control Register*  (AIRC).

The **preempt priority** level defines whether an interrupt can be serviced when the processor is already running another interrupt handler. In other words, preempt priority determines if one interrupt can preempt another.

The **subpriority** level value is used only when two exceptions with the same preempt priority level are pending (because  interrupts are disabled, for example). When the interrupts are re-enabled, the exception with the lower subpriority (higher urgency)  will be handled first.

> I'd recommend to assign all the interrupt priority bits to the preempt priority group, leaving no  priority bits as subpriority bits - default configuration after reset. Other configurations complicate unnecessarily the interrupt handling in most cases.

### Disable and enable interrupts

Most real-time embedded devices need to perform certain operations **atomically** to prevent data corruption. The simplest solution is to briefly disable and re-enable interrupts.

The *ARM Cortex-M* offers two methods of disabling and re-enabling  interrupts. 

- PRIMASK register
- BASEPRI special register

The PRIMASK register offers simple method to disable/re-enable interrupts by set and clear the interrupt bit. To disable the interrupts the `CPSID i` instruction is used. Similarly to re-enable interrupts the `CPSIE i` instruction is used. Note these instructions are available only in the privilege mode and affect all interrupts. This is the only method available in the *ARMv6-M* architecture (*Cortex-M0/M0+*).

```rust
/// All exceptions with configurable priority are either active or inactive
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Primask {
    Active,
    Inactive,
}

/// Get PRIMASK
/// Wrapper to get PRIMASK
#[inline(always)]
fn _primask() -> Primask {
    let primask: u32;
    unsafe { asm!("mrs $0, PRIMASK":"=r"(primask)::: "volatile"); }

    if primask & (1 << 0) == (1 << 0) {
        Primask::Inactive
    }
    else {
        Primask::Active
    }
}
```

However, the newer *ARMv7-M* (*Cortex-M3/M4/M4F*) provides additionally the *BASEPRI* special register, which allows you to disable interrupts only with urgency lower than a certain level and leave the higher-urgency interrupts not disabled at all.

```rust
/// All exceptions with configurable priority are either inactive if their priority is higher than the value of BASEPRI

#[inline(always)]
fn _basepri_max(u32) {
    // TODO: Handle also CM7-r0p1
    let basepri_max: u32;
    unsafe{ asm!("msr BASEPRI_MAX, $0"::"r"(basepri_max)); }
    basepri_max
}

#[inline(always)]
fn _basepri_r() -> u32 {
    // TODO: Handle also CM7-r0p1
    let basepri: u32;
    unsafe { asm!("mrs $0, BASEPRI":"=0"(basepri));}
    basepri
}

/// Basepri
#[inline(always)]
fn _basepri_w(basepri: u32) {
    // TODO: Handle also CM7-r0p1
    unsafe { asm!("mrs BASEPRI, $0"::"r"(basepri)); }
}
```