# Assemble

**TODO: The Rust allocator interface.**

**TODO: http://infocenter.arm.com/help/topic/com.arm.doc.dai0298a/DAI0298A_cortex_m4f_lazy_stacking_and_context_switching.pdf**

## Inline assembly in Rust

In order to access the special registers we'll need a bit of assembly. There are three ways how to achieve this. 

- We can create a new file with the `.S` extension where we put our assembly code and then we'd  create another file called `build.rs` where we invoke the assembler to generate the object file which would then be linked into the final binary. Apart from the `.S` file we would need to define also FFI interface for the assembly subroutines to be actually callable from Rust.
- The second solution doesn't involve using the `build.rs`. It leverages a feature in Rust called `global_asm` which gives us the macro `global_asm!(include_str!("assembly.S"))`. This macro then passes the assembly file through the `rustc` and `llvm` to the assembler. However we need to enable the feature in the root of our crate `#![feature(global_asm)]`.
- The last option is to leverage another feature called `asm` which allows use to use inline assembly in our code through macro `asm!()`.

As we don't have any legacy code we will use inline assembly. The former two approaches might be better if you already have some assembly code that you can plug directly into your Rust application. The reason is that these files are provided by the platform vendor or you already spend some time on coding them. This concernes mainly the startup code (as the C enviroment is not fully initiallized), access to special registers, some algorithms optimizedby hand for high performance. However this approach can have also some drawbacks as you don't have access form Rust to variables defined in the assembly file and the assembly file itself might use also some C defines and e.g. structs defined in C header files resulting in duplication of these symbols also in the Rust source code.  

### asm! macro

So without further ado let's look at the inline assembly macro:

```rust
asm!(assembly template
   : output operands
   : input operands
   : clobbers
   : options
   );
```

- assembly template - The only required parameter, contains instructions and operands
- output operands - Mutable lvalues or not yet assigned
- input operands - Initialized lvalues
- clobbers - Registers that will be indirectly modified.
- options:
  - volatile - do not optimize
  - alignstack - align stack

Any use of this macro requires use of unsafe block. You can read more about inline assembly in the [Rust book].

**TODO: Some examples**

## Call conventions

Let's focus now on the call convention of function regardless of the language be it either C or Rust:

```rust
fn simple(first: u32, second: u32, third: u32, fourth: u32) {}

...


simple(1,2,3,4);
```

On the x86 architecture you would see something similar to this:

```asm
  subl $16, %esp
  movl $4, 12(%esp)
  movl $3, 8(%esp)
  movl $2, 4(%esp)
  movl $1, (%esp)
  call _simple
```

So the function arguments are being pushed, the call will also push the return address (i.e. the program counter of the next instruction after the call) and, what in x86 terms, is often referred to as the saved frame pointer on to the stack. The frame pointer is used to reference local variables further stored on the stack.

### AAPCS

The x86 is a CISC architecture, whereas ARM is a RISC architecture. There is a standard called [AAPCS] or *Procedure Call Standard for the ARM Architecture* describing the *Application Binary Interface* (ABI). Now we look at the assembly:

```asm
MOV      r3,#4
MOV      r2,#3
MOV      r1,#2
MOV      r0,#1
BL       simple
```

The four function arguments will be placed in `r0-r3` registers followed by the *Relative branch with link instruction*. From the listing we see the ARM architecture doesn't use stack for the four parameters and the return address is stored in the *Link register* `r14` rather than pushing all of them to the stack.

What would happen with fifth argument?

```rust
fn advanced(first: u32, second: u32, third: u32, fourth: u32, fifth: u32){}

advanced(1,2,3,4,5);
```

Here is the appropriate listing:

```asm
        MOV      r0,#5
        MOV      r3,#4
        MOV      r2,#3
        STR      r0,[sp,#0]
        MOV      r1,#2
        MOV      r0,#1
        BL       advanced
```

We notice the `STR` instruction suggesting that the fifth parameter of the function is stored on stack as would the sixth and other parameters be stored.

#### Return value

Ok, what about the return values? Let's define one with a return value:

```rust
fn there_and_back(first: u32, second: u32, third: u32, fourth: u32) -> u32{ first + second + third + fourth }

there_and_back(1,2,3,4);
```

The listing for the function:

```asm
MOV      r3,#4
MOV      r2,#3
MOV      r1,#2
MOV      r0,#1
BL       there_and_back
LDR      r1,|L0.40|  ; load address of extern val into r1
STR      r0,[r1,#0]  ; store function return value in val
```

#### 64-bit interger arguments (u64)

Cortex-M architecture operates natively with `u32` variables. But how does it work with longer `u64` ?

```rust
fn too_long(first: u64, second: u64) -> u64 { first + second }

too_long(1000,2000);
```

Compiles into:

```asm
   ...
   LDR      r0,|L0.40|
   LDR      r1,|L0.44|
   LDRD     r2,r3,[r0,#0]
   LDRD     r0,r1,[r1,#0]
   BL       test_ll
   LDR      r2,|L0.48|
   STRD     r0,r1,[r2,#0]
   ...
|L0.40|
   DCD      ll_p2
|L0.44|
   DCD      ll_p1
```

We can see from the listing that `u64` variables take two registers (`r0-r1` for the first parameter and `r2-r3` for the second). The return value is stored in similar fashion in `r0-r1`.

#### Doubles (f64)

As doubles are not supported by the hardware the compiler has to use software emulation. I'll skip this part.

#### Mixing 32-bit and 64-bit variables

In the next example we pass mixture of `u32` and `u64` variables:

```rust
fn mix_instructions(first: u32, second: u32, third: u64) { }

mix_instructions(1,2,1000);
```

Turns out to be:

```asm
 ...
       LDR r0,|L0.32|
       MOV r1,#2
       LDRD r2,r3,[r0,#0]
       MOV r0,#1
       BL test_iil
       ...
|L0.32|
       DCD ll_p1
```

As expected we get first stored in `r0`, second in `r1` and third in `r2-r3`.

Let's now change the order:

```rust
fn mix_instructions(first: u32, third: u64, second: u32) { }

mix_instructions(1,1000,2);
```

The disassembly will look like this:

```asm
     ...
      MOV r0,#2
      STR r0,[sp,#0] ; store parameter b on the stack
      LDR r0,|L0.36|
      LDRD r2,r3,[r0,#0]
      MOV r0,#1
      BL test_ili
      ...
|L0.36|
      DCD ll_p1
```

The parameters will be passed differently: first in `r0`, second in `r2-r3` and third will be stored on stack which might also include an extra alignment operation.

The answer to this lies in AAPCS:

> *A double-word sized type is passed in two consecutive registers (e.g., r0 and r1, or r2 and r3). The content of the registers is as if the value had been loaded from memory representation with a single LDM instruction*

The complier is not allowed to rearrange parameter ordering, therefore unfortunately the parameter *third* has to come in order after *second* and cannot use the unused register r1 - ending up on the stack.

#### OOP

The last section should be pretty important to anyone using object oriented programming. The methods of object (`struct` in Rust) pass implicitly `self` parameter as a `usize` (32-bit pointer) in `r0`. I hope you can realise the implications:

```rust
struct Object{}

impl Object
{
    fn do_worse(&self, first: u64, second: u32);
}
```

vs.

```rust
impl Object
{
    fn do_better(&self, first: u32, second: u64);
}
```

## Back to Cortex-M special registers

No that we can put the knowledge of assembly in Rust code to good use. We will define functions to read write to the PSR register.

```rust
fn _psr_r () -> u32 {
    let psr: u32;
    unsafe { asm!("mrs $0, PSR": "=r"(psr):::"volatile"); }
	psr
}

fn _psr_w (psr: u32) {
    unsafe { asm!("mrs $0, PSR":: "r"(psr)::"volatile"); }
}
```

We use `$0` instead of directly referencing one of the R0-R12 registers so that the compiler can decide which register it will use instead of pushing the content first to stack and then poping it back after our subroutine is done. We also use `volatile` as option parameter as the instruction modifies memory (PSR register).

## Let's structure our code

We could write similar functions for accessing the BASEPRI, PRIMASK and FAULTMASK registers. However, let's structure our code a little bit. We move all code to separate files in module called `cortex`. In this module we crate file `mod.rs` with following content:

```rust
// mod.rs
pub mod psr;
pub mod basepri;
pub mod control;
pub mod faultmask;
pub mod msp;
pub mod primask;
pub mod psp;
```

### Program Status Register (PSR)

If we have a look at PSR we'll see it's 32 bit register that combines:

- *Application Program Status Register* (APSR)
- *Interrupt Program Status Register* (IPSR)
- *Execution Program Status Register* (EPSR)

This means we can create an enum:

```rust
// psr.rs
// Program Status Register
#[derive(Clone, Copy, Debug)]
pub enum Psr {
    Psr(u32),
    Apsr(u32),
    Ipsr(u32),
    Epsr(u32),
}

impl Psr {
    fn read_psr() -> Psr {
        let psr: u32;
        unsafe {
            asm!("mrs $0, PSR": "=r"(psr):::"volatile");
        }
        Psr::Psr(psr)
    }

    fn read_apsr() -> Psr {
        let apsr: u32;
        unsafe {
            asm!("mrs $0, APSR": "=r"(apsr):::"volatile");
        }
        Psr::Apsr(apsr)
    }

    fn read_ipsr() -> Psr {
        let ipsr: u32;
        unsafe {
            asm!("mrs $0, IPSR": "=r"(ipsr):::"volatile");
        }
        Psr::Ipsr(ipsr)
    }

    fn read_epsr() -> Psr {
        let epsr: u32;
        unsafe {
            asm!("mrs $0, EPSR": "=r"(epsr):::"volatile");
        }
        Psr::Epsr(epsr)
    }

    pub fn bits(&self) -> u32 {
        match *self {
            Psr::Psr(psr) => psr,
            Psr::Apsr(apsr) => apsr,
            Psr::Ipsr(ipsr) => ipsr,
            Psr::Epsr(epsr) => epsr,
        }
    }
}

impl Psr {
    pub fn negative(&self) -> Option<bool> {
        match self {
            Psr::Apsr(bits) => {
                if bits >> 31 & 1u32 == 1u32 {
                    Some(true)
                } else {
                    Some(false)
                }
            }
            _ => None,
        }
    }

    pub fn zero(&self) -> Option<bool> {
        match self {
            Psr::Apsr(bits) => {
                if bits >> 30 & 1u32 == 1u32 {
                    Some(true)
                } else {
                    Some(false)
                }
            }
            _ => None,
        }
    }

    pub fn carry(&self) -> Option<bool> {
        match self {
            Psr::Apsr(bits) => {
                if bits >> 29 & 1u32 == 1u32 {
                    Some(true)
                } else {
                    Some(false)
                }
            }
            _ => None,
        }
    }

    pub fn overflow(&self) -> Option<bool> {
        match self {
            Psr::Apsr(bits) => {
                if bits >> 28 & 1u32 == 1u32 {
                    Some(true)
                } else {
                    Some(false)
                }
            }
            _ => None,
        }
    }

    pub fn saturation(&self) -> Option<bool> {
        match self {
            Psr::Apsr(bits) => {
                if bits >> 27 & 1u32 == 1u32 {
                    Some(true)
                } else {
                    Some(false)
                }
            }
            _ => None,
        }
    }
}
```

```rust
impl Psr {
    pub fn isr_number(&self) -> Option<u8> {
        match self {
            Psr::Ipsr(bits) => Some(*bits as u8),
            _ => None,
        }
    }
}
```

We can write similar functions for accessing the BASEPRI, PRIMASK and FAULTMASK registers:

```rust
fn _primask () -> u32 {
	let primask: u32;
    unsafe { asm!("mrs $0, PRIMASK":"=r"(primask)::: "volatile");
    primask
}
```

##  