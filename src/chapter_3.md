# Clear the tower

**TLDR;**

The goal of this chapter is provide a reference *Rust* project that will compile and run on *ARM Cortex-M4* microcontroller. We'll implement all the low level stuff, talk about cross-compilation and finally flash the application (just neverending loop though) to the hardware or how to run it in *QEMU*.

## Time to unload some Cargo
With all the necessary introduction and theory covered let's get to the command line and start working on our bare metal application.

First create a new *Rust* project:
```shell
$ cargo new --bin cortex
	Created binary (application) `cortex` project
```

Next we need to specify that our application will be freestanding - there is (un)fortunately neither an operating system nor a [BIOS] running (nor [Intel ME]) on the microcontroller to facilitate certain services e.g. print to screen, get input, etc. To turn our (probably desktop) application into a bare metal one we'll use crate attribute `#![no_std]` which must be added to the top of our crate root - `main.rs`.

```rust
// main.rs
#![no_std]

fn main() {
    println!("Hello, world!");
}
```
By introducing crate attribute `#![no_std]` we removed the dependncy on standard library and we'll link to the `core` crate instead. The `core` crate is platform-agnostic supporting only subset of standard crate lacking anything that requires platform specific API (like dynamic memory allocation, stakc overflow protection, etc.) making it ideal to write bootstrapping code - bootloaders, firmware or kernel. There is even a [Request For Comment] to stabilize `#![no_std]` crate attribute. Let's build (using the `cargo build`) the application:
```shell
$ cargo build
error: cannot find macro `println!` in this scope
 --> src/main.rs:4:5
  |
4 |     println!("Hello, world!");
  |     ^^^^^^^
```
Unfortunately removing the standard library means we'll be unable to use the `println!` macro as it's part of the standard library. Therefore we have to remove the print statement (don't worry, we'll implement it later)

> The `println!` macro implementation writes to a file descriptor provided by the operating system. To open/read/write/close a file we need to call a particular system service.

## Don't panic!
So without the print statement we build the application again.
```shell
$ cargo build
error: language item required, but not found: `panic_fmt`

error: language item required, but not found: `eh_personality`

error: aborting due to 2 previous errors
```
Now  we get errors from the `rustc` compiler about some [language items]. What are those?

Language items are part of the standard library and are called under certain conditions e.g. when the application `panics`.

We could implement our own language items. They are, however, highly unstable part of *Rust* language. Is there a better and more stable solution?

`eh_personality` is responsible to unwind the stack when `panic` occurs. The stack unwind means that any temporary values, local variables, or arguments that the current function was using are dropped, in the reverse order they were created. Dropping a value simply means cleaning up after it. In case of `String`,  `Vec` or `Box` it means the resource allocated on heap is freed. Structures implementing the `drop` method get called e.g. any open Files are closed.

> If a `drop` method triggers a panic while Rust is trying to clean up after the first, then it is considered fatal - the whole process aborts. 

Our implementation would be too difficult as the implementation itself is OS specific and we don't even have an OS. Therefore we'll just abort on panic by adding the following line to `Cargo.toml`:

```toml
# Cargo.toml

[profile.dev]
panic = "abort"

[profile.release]
panic = "abort"
```
> As an alternative we could specify the same option but in a target specification file. There we would add `"panic-strategy": "abort"` instead of adding it to `Cargo.toml`. But let's not jump ahead.

> There also additional profile `[profile.test]`

**TODO: Add debug symbols in release - debug=true, the -g param is passed to rustc**

The second language item is `panic_impl`. As the name implies it is the function that is called on panic. Again we could implement it ourselves or we could have a look on [RFC2070] describing panic implementation - The `panic_implementation` has been recently stabilized.

~~Since the `panic_implementation` has been added quite recently in *Rust* and has therefore been still unstable, we'll need another crate attribute called *feature gating*. This attribute enables us to use features that have been added to the language but till now are considered unstable. Again we must put the crate attribute to the top of our crate root and add the implementation of function `panic`:~~

```rust
// main.rs
#![no_std]

use core::panic::PanicInfo;

fn main () {}

#[panic_handler]
#[no_mangle]
pub fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
```

~~In order to use *features* we'll also need to switch to the *Rust* nightly compiler as the features are not available in the stable or beta [releases].~~

Perhaps the term `panic` for the function is a little misleading as it's an orderly process - not undefined behavior. Panicking is safe way to catch e.g. an invalid array access, before anything bad happens. It would be unsafe to proceed further so Rust unwinds the stack.

**TODO: Put unwinding after panic to gently continue.**

We'll switch also to *Rust* nightly compiler as we'll need it later to compile the core crates later even though we won't have to enable any *features*. So we'll use `rustup` and in the project directory we'll execute:

```shell
$ rustup override set nightly
info: using existing install for 'nightly-x86_64-unknown-linux-gnu'
info: override toolchain for 'C:<..>\cortex_guide\cortex' set to 'nightly-x86_64-unknown-linux-gnu'

  nightly-x86_64-unknown-linux-gnu unchanged - rustc 1.29.0-nightly (6a1c0637c 2018-07-23)

```

This will create an entry in `.rustup/settings.toml` under section `[overrides]`.

```toml
[overrides]
"/<PATH>" = "nightly-x86_64-unknown-linux-gnu"
```

## Rust runtime

Now have the language items now implemented and compiler switched to nightly version, we build again the project:

```shell
$ cargo build
error: requires `start` lang_item
```

It might be strange but the `main` is not the first function that is called either on desktop or embedded. You might ask why? The answer is the missing language item.

What does the `start` do? The `start` is responsible for initialization of language runtime system - like garbage collection, software threads or virtual machine. At least it needs to initialize the variables in RAM and setup the stack.

>  [The Guide to the Rust Runtime] states *Rust includes two runtime libraries in the standard distribution, which provide a unified interface to primitives such as I/O, but* **the language itself does not require a runtime**. *The compiler is capable of generating code that works in all environments, even kernel environments* .

> You can find out more about [How rust works without the standard library] 

For now we'll leave that function empty. In later chapter we'll learn about boot process and how to initialize the variables in RAM. As we intend to implement our own `_start` function we need to use another crate attribute `no_main`:

```rust
// main.rs
#![feature(panic_implementation)]
#![no_std]
#![no_main]

...

pub extern "C" fn _start() -> !{

  main();
  loop{}
}
```
Build with:

```shell
$ cargo rustc -- -Z pre-link-arg=-nostartfiles
```

If you build your application on Linux you'll find the binary in `target/debug/cortex`. We can inquire the finished executable through `readelf`:

```shell
$ readelf -a target/<target-name>/debug/cortex
ELF Header:
  Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF64
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           Advanced Micro Devices X86-64
  Version:                           0x1
  Entry point address:               0x280
  Start of program headers:          64 (bytes into file)
  Start of section headers:          9328 (bytes into file)
  Flags:                             0x0
  Size of this header:               64 (bytes)
  Size of program headers:           56 (bytes)
  Number of program headers:         8
  Size of section headers:           64 (bytes)
  Number of section headers:         21
  Section header string table index: 2
```

This is the header of the executable - [ELF]. But there is a catch. The binary is targeted for `x86_64` architecture (see Machine). It's time to give cross-compilation a try.

> If you run on Windows (or Mac OS) you'll probably get errors as the `start` symbol is actually called `mainCRTStartup` (Mac OS needs to link `libSystem`).

## Target specification
Ok. So we've just built the crate for the architecture of your laptop or other metal darling. In my case it's `x86_64`. However the *Cortex-M* has `ARMv7-M` architecture. So how do we tell the compiler we wish to build the crate for different architecture?

To compile the executable for the target architecture *Rust* uses so-called *target specification* files. These use JSON format and specify the target architecture.

>To *cross-compile* is to build on one platform a binary that will run on another platform

To obtain one we can invoke the `rustc` compiler passing in these parameters: 

```shell
$ rustc +nightly -Z unstable-options --print target-spec-json
```

The output will depend on your architecture. I'm using *x86_64*:

```json
{
  "arch": "x86_64",
  "cpu": "x86-64",
  "data-layout": "e-m:e-i64:64-f80:128-n8:16:32:64-S128",
  "dynamic-linking": true,
  "env": "gnu",
  "exe-allocation-crate": "alloc_jemalloc",
  "executables": true,
  "has-elf-tls": true,
  "has-rpath": true,
  "is-builtin": true,
  "linker-flavor": "gcc",
  "linker-is-gnu": true,
  "llvm-target": "x86_64-unknown-linux-gnu",
  "max-atomic-width": 64,
  "os": "linux",
  "position-independent-executables": true,
  "pre-link-args": {
    "gcc": [
      "-Wl,--as-needed",
      "-Wl,-z,noexecstack",
      "-m64"
    ]
  },
  "relro-level": "full",
  "stack-probes": true,
  "target-c-int-width": "32",
  "target-endian": "little",
  "target-family": "unix",
  "target-pointer-width": "64",
  "vendor": "unknown"
}
```

To get the target specification for our core we need to pass `--target thumbv7em-none-eabi`

```json
{
  "abi-blacklist": [
    "stdcall",
    "fastcall",
    "vectorcall",
    "thiscall",
    "win64",
    "sysv64"
  ],
  "arch": "arm",
  "data-layout": "e-m:e-p:32:32-i64:64-v128:64:128-a:0:32-n32-S64",
  "emit-debug-gdb-scripts": false,
  "env": "",
  "executables": true,
  "is-builtin": true,
  "linker": "arm-none-eabi-gcc",
  "linker-flavor": "gcc",
  "llvm-target": "thumbv7em-none-eabi",
  "max-atomic-width": 32,
  "os": "none",
  "panic-strategy": "abort",
  "relocation-model": "static",
  "target-c-int-width": "32",
  "target-endian": "little",
  "target-pointer-width": "32",
  "vendor": ""
}
```

Here are some other relevant targets to explore:

* `thumbv6m-none-eabi` - *Cortex-M0* & *Cortex-M0+*

- `thumbv7m-none-eabi` - *Cortex-M3*

- `thumbv7em-none-eabi` - *Cortex-M4* & *Cortex-M7 without a FPU*

- `thumbv7em-none-eabihf` - *Cortex-M4* & *Cortex-M7 with a FPU*

## Target specification fields

Ok. So the target specification file has a lot of fields? What do they mean? Well, you can find the documentation in the [official documentation] under these two structs:

```rust
Struct rustc_target::spec::Target
Struct rustc_target::spec::TargetOptions
```

Note that we the llvm-target contains none as an OS target and the os field is also set to none, because we will run on bare metal.
We'll use the *LLD* linker that is shipped with Rust to minimize the dependency on external tools.
As we do not currently support unwinding we'll set panic-strategy to abort which has the same effect as option `panic = "abort"` in `Cargo.toml`.

The `llvm-target` field specifies the target triple that is passed to *LLVM*. [Target triples] are a naming convention that define the CPU architecture (e.g., `x86`, `x86_64` or `arm`), the vendor (e.g., `apple` or `unknown`), the operating system (e.g., `windows` or `linux`), and the [ABI] (e.g., `gnu` or `msvc`). For example, the target triple for 64-bit Linux is `x86_64-unknown-linux-gnu` and for 32-bit Windows the target triple is `i686-pc-windows-msvc`.

The `data-layout` field is passed to *LLVM* and specifies how data should be laid out in memory. The specification can be found in the [LLVM documentation] but there shouldn't be a reason to change this string.

The `linker-flavor` field is intended to add support for the *LLVM* linker [LLD],  which is platform independent. This removes the need to have installed a *GCC* cross compiler for linking.

## Cross-compile using xbuild

In our `main.rs` we specified that we don't want to use the *Rust Standard Library*. Nevertheless we'll need the *Rust Core Library* -  defines the intrinsic and primitive building blocks of all *Rust* code. To build this library and to cross-compile our crate we'll use crate `cargo-xbuild` which internally uses `xargo` . `xargo` is a wrapper around cargo and facilitates the build of *Rust Core Library*.

Type this command to get *Rust* target for `thumbv7m`:

```shell
$ rustup target add thumbv7m-none-eabi
```

Then install `cargo-xbuild`:

```shell
$ cargo install cargo-xbuild
```

Last we need to specify the target we want to build in `.cargo/config`. We can select to use either GNU GCC or LLVM LLD linker by commenting out the lines:

```toml
# .cargo/config

[build]
target = "thumbv7em-none-eabi"

[target.thumbv7em-none-eabi]
runner = 'arm-none-eabi-gdb'
rustflags = [
  # uncomment to use GNU GCC linker
  #"-C", "link-arg=-Wl,-Tlayout.ld",
  #"-C", "link-arg=-nostartfiles",

  # uncomment to use rustc LLD linker
   "-C", "link-arg=-Tlayout.ld",
   "-C", "linker=lld-link",
   "-Z", "linker-flavor=ld.lld",
]
```

Now finally you can cross-compile the crate running:

```shell
cargo xbuild --target thumbv7m-none-eabi
```

Damn it. We are now missing a linker script. It's time to write one.

> During compilation we can see three crates that are compiled - the Core library, the Allocation library and the Collection library.

### 

[]: 