 # Octopus, new hope

## Direct Memory Access

DMA is a special controller capable of accessing the system bus. It enables the movement of blocks of data from peripheral to memory, memory to peripheral, or memory to memory without burdening the processor. That means the CPU can do other things in the meantime instead of just manually loading data from memory into registers and then storing them back (to different address) - a kind of asynchronous `memcpy`. Even though the DMA transfers offers performance and use case benefits they can be o the other hand dangerous when misused.

The first scenario concerns itself when the buffer is allocated on stack. The reason why we would do this could be that we are not allowed to use dynamic memory allocation and the memory can't be statically allocated. The issue here is of course that the buffer is deallocated and the memory of former buffer is reused for other variables. The consequence is that these variables are then asynchronously overwritten.

Here the problem is that a transfer is started on a stack allocated buffer but then the buffer is
immediately deallocated. The call to `corrupted` reuses the stack memory *that the DMA is operating
on* for the stack variables `x` and `y`; this lets the DMA overwrite the values of `x` and `y`,
wreaking havoc. If you add optimization into the mix it becomes impossible to predict what will
happen at runtime.

Therefore we'll need to define the Transfer the DMA operates on.

```rust
/// Ongoing DMA transfer
struct Transfer<'a> {
    buf: &'a mut [u8],
    ongoing: bool,
}
```

The lifetime of the buf prevents as from making the programer mistake.

## CPU and DMA master bus access

## Sync vs Async transfer

I'd like to end this post by discussing the differences between synchronous (CPU) and asynchronous (DMA) transfer and where to apply them.

It's true that the synchronous transfer is a burden on your CPU, but if you need to do some processing on the data along the way the there is no other way. Also if you look at our `reset_handler` you'll see initialization of RAM with CPU. Why not DMA? Well, for one the RAM is not initialized