# Map to treasure island
**TLDR;**

With the implemented *Semihosting* for debug purposes we are no ready to dive deep into the memory architecture of *Cortex-M4*. As the peripherals are controlled by just read and write to certain addresses we'll have to come up with a way to build a friendly yet safe API.

## Bus architecture

*Cortex-M4* has several bus interfaces allowing the processor to fetch instructions and access data at the same time. There are following bus interfaces:

- ICode memory interface
- DCode memory interface
- System interface
- Private peripheral bus

ICode memory interface fetches instructions from code memory space from address range of `0x00000000` to `0x1FFFFFFF`.

DCode memory interface provides data and debug access within `0x00000000` to`0x1FFFFFFF`

System interface provides Instruction fetches, and data and debug accesses to address ranges `0x20000000` to `0xDFFFFFFF` and `0xE0100000` to `0xFFFFFFFF`. It also  contains control logic to handle unaligned accesses, FPB remapped accesses, bit-band accesses, and pipelined instruction fetches.

Private peripheral bus provides data and debug accesses to external PPB space, `0xE0040000` to `0xE00FFFFF`. The *Trace Port Interface Unit* (TPIU) and vendor specific peripherals are implemented on this bus.

## Memory map

The memory map spans linearly from `0x00000000` to `0xffffffff`. The 4 GB in total of memory space is divided into predefined ranges. This allows the built-in peripherals, such as the interrupt controller to be accessed by simple memory access instructions. Nevertheless due to the the flexibility of the architecture the predefined regions can be used differently e.g. copy the `CODE` section from FLASH region to RAM and execute it from there. This would improve the speed of instruction fetching despite loosing the benefit of separate data & instruction bus).

## Memory Protection Unit

Depending on the vendor an *Memory Protection Unit* (MPU) peripheral can be bundled to the processor.  The MPU is a simplification of a `Memory Management Unit` MMU which handles translation between virtual and physical memory - therefore preventing process from accessing memory not associated with it. As said the task of MPU is much simpler. It only allows to set rules for privileged and user access. Upon rule violation the fault exception is generated and exception handler executed to analyze the fault.

There are various ways how to configure the MPU:

- OS - protect kernel data from user process
- Read protection - protect memory from being accidentally overwritten
- Detect stack overflow - Upon stack overflow generate exception

When an access rule is violated, a fault exception is generated, and the fault
e be able to analyze the problem and correct it, if possible

## Port-mapped vs Memory-Mapped Input/Output

Before we can create our Hello World application - blinking LED or printing over UART (if you wish to see the "Hello World!" string in your terminal) we need to cover the topic of accessing the peripherals. There are two complementary methods of performing input/output between the CPU and I/O devices in a target: *Memory-Mapped Input/Output* (MMIO) and *Port-mapped Input/Output* (PMIO).

PMIO requires use of a special class of CPU instructions designed to performing I/O. This is main I/O method found on Intel microprocessors, specifically the IN and OUT instructions which can read and write a single byte/half-word/word to an I/O device. The address space of the I/O devices is separated from general memory. This is either accomplished by an extra “I/O” pin on the CPU’s physical interface, or an entire bus dedicated to I/O.

PMIO is usually accessed via special compiler-specific intrinsic functions - non-standard part of *Rust* language. As PMIO is not present on *ARM Cortex-M* microcontrollers I'll end with the PMIO description here.

MMIO uses the same bus to address both memory and I/O devices. The CPU instructions used to read and write to memory are also used in order to access I/O devices. The chip manufacture must reserve part of the addressable space for I/O rather than memory. The I/O devices monitor the CPU’s address bus and respond to any CPU access of their assigned address space, mapping the address to their hardware registers.

These two techniques are usually not found together on the same architecture (there might be some exceptions like older PCs where the video RAM would me memory mapped and all other I/O devices would use port I/O).

## Accessing memory-mapped hardware form Rust

We face a problem! From the perspective of the *Rust* language the compiler will only see objects that are declared in the memory areas. There is no direct way of accessing I/O addresses from Rust but though using pointers indirectly. Dereferencing pointers in Rust is considered dangerous and requires us to use  the keyword `unsafe` as the dereferencing instruction will go in this block.

So we need to:

- Select the correct type of the pointer

> Tells compiler how many bytes we want to access, how to interpret the bits and wether we can read/write to the region

- *Force* the address into the pointer

> Note: All access to the registers should be done through the unsigned types.

Therefore we can define the following functions:

```rust
fn reg_8  (addr: *const u32) -> u8 { ... }
fn reg_16 (addr: *const u32) -> u16 { ... }
fn reg_32 (addr: *const u32) -> u32 { ... }
```

However, note that using a pointer-to-32-bit to access an 8-bit register is undefined. Doing so you risk overwriting adjacent registers; and even reads could have unintended side-effects (e.g. reading some hardware registers has the effect of clearing them). We'll address this issue later with Rust's type safety and macros.

Since our hardware registers are at fixed locations in memory (and should never change!) we can improve the optimizing capacity of the compiler by making the pointers constant.

> Please remember that we make the address of the register `const` rather than the content of the register which is free to change.

Now let's look at the implementation part:

```rust
fn reg_8  (addr: *const u32) -> u8 {
    *addr
}

fn reg_16 (addr: *const u32) -> u16 {
    *addr
}

fn reg_32 (addr: *const u32) -> u32 {
    *addr
}
```

It might look like as simple as just dereferencing a raw pointer. If we try to compile the code we get an error `error[E0133]: dereference of raw pointer is unsafe and requires unsafe function or block`. We can find out more by querying the `rustc` compiler: 

```shell
$ rustc --explain E0133
Unsafe code was used outside of an unsafe function or block.

Erroneous code example:

​```
unsafe fn f() { return; } // This is the unsafe code

fn main() {
    f(); // error: call to unsafe function requires unsafe function or block
}
​```

Using unsafe functionality is potentially dangerous and disallowed by safety
checks. Examples:

* Dereferencing raw pointers
* Calling functions via FFI
* Calling functions marked unsafe

These safety checks can be relaxed for a section of the code by wrapping the
unsafe instructions with an `unsafe` block. For instance:

​```
unsafe fn f() { return; }

fn main() {
    unsafe { f(); } // ok!
}
​```
```

So the `rustc` tells us that dereferencing a raw pointer is a potentially dangerous operations and rust safety checks can't be applied here. Therefore we have to relax the safety check by putting the dereferencing operation into unsafe block. The responsibility of the code inside the unsafe block is on the side of the programmer. The reason here is completely justified as e.g. dereferencing memory outside of RAM, FLASH or Peripheral region will cause Hard Fault (Memory management if enabled) handler to be executed.

The solution here would then be make the functions unsafe and make the caller put unsafe block around the invocation. 

However, this approach **is not the way we want to do things in Rust**! It's very easy to mess up when working with raw pointers inside unsafe blocks. So we want to minimize the use of `unsafe` as much as  possible. Rust gives us the ability to do this by creating safe abstractions. 

### Registers and side effects

We should not forget that the value of hardware registers depends on the current state of the hardware (not necessarily by the actions of the program).

- When a hardware register is written the value read back might yield a different value rather than the same.
- Two sequential reads from register might also yield a different results.
- Due to optimizations of the code the compiler might optimize the code by removing or reorganizing the instructions

We can explore the last issue in the following code:

```rust
  let cfg1 = reg8(0x40000000);
  let cfg2 = reg8(0x40000001);
  let data = reg8(0x40000002);
  ...

  while(data == 0)
  {
    // Wait for data to arrive...
  }
```

The code could *hang* especially at higher optimisation levels.
This also applies to writes to memory which is not read.

```rust
...
cfg1 = 0xbe;
cfg2 = 0xef;
...
```

To overcome these issues in *C* we would use keyword `volatile` that tells compiler not to reason about the variable (it doesn't define a special memory type).
However, in *Rust* there is no `volatile` modifier. Instead we have to rely on *volatile read* and *volatile write* primitives in the *Rust Standard Library*. Notice the required safety block as we access memory directly.

```rust
use std::ptr::{write_volatile, read_volatile};

...
let reg = 0x40000000;
let ptr = &mut reg as *mut u32;
let val = 0xdeadbeef;
unsafe {
    write_volatile(ptr, val);
    assert_eq!(read_volatile(ptr), val);
}
```

### Tidying up

To improve the readability of the code we'll use the `volatile` crate and add it as an dependency to our `Cargo.toml`:

```toml
# Cargo.toml

[dependencies]
volatile = "0.2.3"
```

In `main.rs` we import the crate and use it:

```rust
extern crate volatile;
use volatile::Volatile;

...

let ctrl_reg = 0x40000000 as *mut Volatile<u32>;
...
ctrl_reg.write(0xdeadbeef);
```

The `Volatile` type is [generic](https://doc.rust-lang.org/book/second-edition/ch10-00-generics.html) and can wrap almost any type. This ensures that we can't  accidentally write to it through a *normal* write. Instead, we have to  use the `write` method now.

Instead of a normal assignment using `=`, we're now using the `write` method. This guarantees that the compiler will never optimize away this write. Methods internally use the [read_volatile](https://doc.rust-lang.org/nightly/core/ptr/fn.read_volatile.html) and [write_volatile](https://doc.rust-lang.org/nightly/core/ptr/fn.write_volatile.html) functions of the core library.

To improve our code even more we can replace the [magic numbers] with *constants*.

```rust
// Private Constant (address)
const PERIPHERAL_CFG_REG: *mut u32 =0x40000000;

// Public Constant (value)
pub const PERIPHERAL_CFG1: u32 = 0xdead_beef;

...

let ctrl_reg = PERIPHERAL_CFG_REG as *mut Volatile<u32>;
...
ctrl_reg.write(PERIPHERAL_CFG1);
```

### Bit manipulation

Many times we deal with the hardware we need to manipulate the individual bits in the register. We perform these operations:

- Set a particular bit or set several bits
- Clear a bit/bits
- Query if the bit/bits are set

Let's look closely at these operations on read-write register. Read-only and Write-only registers will be covered afterwards.

#### Setting the bits

In order to set the individual bits and leave all the other bits untouched we'll use the bitwise `OR`(|) operation.

| A    | B    | **Y = A OR B ** |
| ---- | ---- | --------------- |
| 0    | 0    | **1**           |
| 0    | 1    | **1**           |
| 1    | 0    | **1**           |
| 1    | 1    | **0**           |

```rust
cfg1_val = cfg1.read();
cfg1.write(cfg1 | 1<<1);
assert_eq!(read_volatile(cfg), cfg1_val | 1<<1);
```

Notice also the *read-modify-write* operation that is now fine when dealing with a register which doesn't change.

> Configuration registers usually don't change due to the state of hardware only through the software in contrast with e.g. status registers which signal the software the state of the hardware. Note that reading the status register might clear the bits. We'll address *read-write-modify* in Bit banding section

#### Clearing the bits

The `OR` operator helps us setting the bits but it can't help us clearing them. We'll need a counterpart - bitwise `AND` operator.

| A    | B    | **Y = A AND B ** |
| ---- | ---- | ---------------- |
| 0    | 0    | **0**            |
| 0    | 1    | **0**            |
| 1    | 0    | **0**            |
| 1    | 1    | **1**            |

```rust
cfg1_val = cfg1.read();
cfg1.write(cfg1 & 1<<0xfe);
assert_eq!(read_volatile(cfg), cfg1_val & 1<<0xfe);
```

To make the code more readable in case of a large register we can use the bitwise `NOT` operator.

```rust
cfg1_val = cfg1.read();
cfg1.write(cfg1 & ~(1<<0));
assert_eq!(read_volatile(cfg), cfg1_val & ~(1<<0));
```

> It's common to specify the bits you want to keep set only to negate them in order to obtain the mask. This operation is done at compile time as the value is const but gives you good readability. But you're free to specify the mask directly in hex if there are more bits to be kept set.

#### Checking a bit

We can use bitwise `AND` operation also to check bits as the AND-ed variable will retain its original value. The result will either be *zero* if our target bit is not set, or *non-zero* if it is set.

```rust
cfg1_val = cfg1.read();

if (cfg1 & ~(1<<0) != 0) {
}
```

#### Read-only registers

These registers are read only and any write access to this register is undefined. Note that e.g. in case of status register the bits might be clear by the read operation.
**TODO: Define a read-only register using volatile crate**

#### Write-only registers

These registers can only be written to. The value read from a write-only register is undefined. Most likely the value will be some random number that does not reflect the state of the register.
Therefore you must never use the bitwise `OR` operator as it could additionally set inproper bits in the register.
**TODO: Define a write-only register using volatile crate with commented out example of bitwise OR**

### Bit-Banding

In last section we covered the topic of setting/clearing bits in register using bitwise operations. I've slightly pointed out that all these operations (except checking the bits) are *read-modify-write* operations.
Before I'll explain what the Bit-Banding is let's look on how the ALU in the core processes data. The CPU (a RISC type in our case) has a load/store architecture. It can't access the memory directly but through loading the content into its registers - this is the first operation *read*. Then it applies logic operation/s to the value in register - the second operation *modify*. Finally it needs to store the modified value back to register- the last operation *write*.

This set of *read-modify-write* operations is mostly fine when doing one thing at a time. However, when we try to do more thing concurrently we'll have to face the case when the value in the register changes during the read and modify operation. The new value will be overwritten by the old one - the race-condition leads to undesired behavior.

So how do we negate this effect? One solution would be to use *atomic* instructions. The term atomic means the value is modified in place as there is support in the hardware. But these instructions are not part of the *ARM Cortex-M4* core.

Nevertheless, there is another solution that is available on *Cortex-M3/M4*  cores. It's called *Bit-Banding*. The device defines a part of the memory known as *Bit-Band* region and maps each bit of an entire word to this region.

The benefit of this approach is that write to the word in the alias region performs a write to the corresponding bit in the register. This also applies to reading a bit which will be read as one word.
These operations take a single machine instruction thus eliminate race conditions. An elegant solution to peripheral registers where it's often necessary to set and clear individual bits.

**TODO: Figure of Bit-Banding figure 4.2.**

http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0337e/CEGHJDCF.html

The Bit-Band region in STM32F4 is located at address of `0x20000000 + <ADDR_OF_PERIPHERAL>`.
**TODO: Add example.**
Though this approach eliminates the *read-modify-write* issue it has also some draw backs. The Bit-Banding is implemented on the bus matrix (probably somewhere close to the address decoder). The compiler itself has no idea about this so you need to define these registers and use them in your code. As the Bit-Banding is feature common to only *Cortex-M3/M4* it should be taken into account when writing a portable code as the *Cortex-M0/0+* and *Cortex-M7* cores do not have it.

> Cortex-M0/0+ are designed to be as simple as possible and Cortex-M7 has complex bus matrix including caches. Therefore these cores don't support Bit Banding as it would complicate the bus matrix.

#### Bit-banding in peripherals

So the Bit-Banding is a useful feature but with its drawback. The atomic instructions need to be also supported by the instruction set. So is there a way to do atomic *read-modify-write*? Well to some extend there is. It can be kind of emulated by the hardware peripheral without the knowledge of the core.

Let's look at GPIO peripheral on STM32F4. In order to change the output voltage on the pin we would have to modify the value in `ODR` (also the port would have to have clocks gated and configured the selected pins as outputs in MODER at least).

Imagine RTOS with two threads running and toggling with the pins controlling LEDs. There are multiple approaches - like using a mutexes or another task and communicating with mailboxes. Though these solution would be viable they would consume a lot of resources(separate thread) or performance(mutexes) compared to following solution.

There is another register called `BSSR` which allows the software to set/clear atomically the bits in ODR. This way both threads can write to the same register without a mutex to get rid of the race-conditions (if the second thread is evil it can modify the bits of the first thread, but that is another topic).

The formula for mapping the bits to bytes is as follows:

```
bit_word_addr = bit_band_base + (byte_offset * 32) + (bit_number * 4)
```

* *bit_band_base* – a constant that indicates which memory area you are trying to access. For SRAM bits use 0x2200 0000, for peripheral registers use 0x4200 0000.
* *byte_offset* – this is a byte offset of register that you are trying to access. Offset is derived from actual address minus starting address of its memory section. Example 1: *byte_offset* of SRAM (SRAM starts at 0x2000 0000) variable located at 0x2000 0100 is 0x2000 0100 – 0x2000 0000 = 0x0000 0100. Example 2: *byte_offset* of *GPIOA->ODR* (address: 0x4002 0014) register is 0x0002 0014, since peripheral memory section starts at 0x4000 0000.
* *bit_number* – just the number of bit you are trying to map. Plain and simple.

To sum up these are the cases where bit abnding might help:

- Clearing Interrupt status bits – saves you some cycles during interrupt routine execution
- Fast GPIO toggling – pretty much explains itself, useful when implementing bit-banging interfaces
- Preemption safe bit value changing – no *read-update-write,* just *write*, good for dealing with data that is processed by interrupt routines and main program loop simultaneously.

You can't use bitbanding on FLASH or set DMA transfers to bit-band memory locations.

**TODO: Add example in Rust**
**TODO: Add link to section (ARM manual)**
This approach does require the support only in the peripheral itself.



[ LInkage ]: https://doc.rust-lang.org/reference/linkage.html
[RFC 911]: https://github.com/rust-lang/rust/issues/24111#issuecomment-323516407
[magic numbers]: https://en.wikipedia.org/wiki/Magic_number_(programming)