 Multitasking

> In computing, [**multitasking**] is the concurrent execution of multiple tasks (also known as processes over a certain period of time.

Multitasking can be divided into two groups:

- Cooperative multitasking
- Preemptive multitasking

### Cooperative

Used by the early multitasking operating systems. The design relies on each process to regularly give up the time to other processes on the system. Therefore, a single bug or inadequately written program can consume most of the CPU time for itself and cause the system to hang.  

### Preemptive 

On the other hand the preemptive multitasking does not rely on the process to hand over execution, but instead is regularly preempted. This guarantees more reliable time slices for each process and makes the system more responsive as well as robust.

## Scheduler

## 

## Task context

## MPU

## JTAG Chain

# Summary

[**multitasking**]: https://en.wikipedia.org/wiki/Computer_multitasking