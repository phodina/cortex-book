# Knock my socks off





## Implementing SysTick

The *ARM Cortex-M* specification allows the vendors to include an optional system timer SysTick. This down counting timer is mainly used in operating systems where interrupts carry out context switching to support multiple task. Without OS the SysTick can be used for time keeping, time measurement or as an interrupt source for periodically executed tasks.

Main benefit is the portability of the application. Certain drawback can be in timer simplicity - e.g. no up counting, missing CAPCOM unit ...

**TODO: Example - Heartbeat interrupt increment every time and then decrement**

## System Control Block

The *System Control Block* (SCB) provides system implementation and control.

- *CPU ID Register* (CPUID)
- *Interrupt Control and State Register* (ICSR)
- *Vector table register offset* (VTOR)
- *Application Interrupt and Reset Control Register* (AIRCR)
- *System Control Register* (SCR)
- *Configuration and Control Register* (CCR)
- *System Handler Priority Register 2* (SHPR2)
- *System Handler Priority Register 3* (SHPR3)



```rust
// scb.rs

/// System Control Block
#[repr(C)]
pub struct SCB {
    /// Configuration and Control Register (0xE000ED14)
    ccr: ReadWrite<u32, CC::Register>,
    /// System Handler Priority Registers (0xE000ED18)
    shp: ReadWrite<u32>,
    /// System Handler Control and State Register (0xE000ED24)
    shcr: ReadWrite<u32, SHC::Register>,
    /// HardFault Status Register (0xE000ED2C)
    hfsr: ReadWrite<u32, HFS::Register>,
    /// Configurable Fault Status Register (0x??)
    cfsr: ReadWrite<u32, CFS::Register>,
    /// MemManage Fault Status Register (0xE000ED28)
    mmfsr: ReadWrite<u32>,
    /// MemManage Fault Address Register (0xE000ED34)
    mmfar: ReadWrite<u32>,
    /// BusFault Status Register (0xE000ED29)
    bsfr: ReadWrite<u32>,
    /// BusFault Address Register (0xE000ED38)
    bfar: ReadWrite<u32>,
    /// UsageFault Status Register (0xE000ED2A)
    usfr: ReadWrite<u32>,
    /// Auxiliary Fault Status Register (0xE000ED3C)
    afsr: ReadWrite<u32>,
    /// Only for Cortex-M7 Auxiiary BysFault Status Register (0xE000ED3C)
    abfsr: ReadWrite<u32>
}
```

## Implementing stub for Hard Fault handler

Yes, we could leave the Hard Fault handler implementation just to neverending loop but that would not help us later to diagnose the error (should any happen). As we don't have printing support yet we'll just implement a small stub that we'll later complete.
However, in order to test the handler we'll need to trigger some fault? Any ideas? I've always wondered what the result of divison by zero is. Well why don't we query the opion of the microcontroller. Perhaps it will get a [Fields Medal]

```rust
let field_medal = 100/0;
```

If we run the code we get, possibly surprisingly, the result zero. Note this is neither the correct answer nor behaviour.

To get the *divide-by-zero* error we need tho enable the bit `DIV_0_TRP` in `CCR`.

Also rather than having to enter the breakpoint via *GDB* I like to add a breakpoint instruction to force the processor into debug mode automatically.

**TODO: Hard fault implementation**

The parameter for the breakpoint shouldn't be `0xAB` which is reserved for *Semihosting*. Any other value works fine.

If we execute the code now we end up in never ending loop inside the Hard Fault handler.

**TODO: Backtrace/unwind**

## MemManag fault

**TODO: MemManag fault implementation**

## Bus fault

**TODO: Bus fault implementation**

## Usage fault

**TODO: Usage fault implementation**

## EXTI

**TODO: Select the Port through SYSCFG_EXTICR1**

### Forward/Reverse

In the last post we implemented roulette where we iterated over the LEDs and lighted them up only to turn off the light in the next iteration. What if we wanted to reverse the direction? Sure ,just call `.iter().rev()` and we reverse the direction. The problem here lies in more the event. Suppose that I want to be able to do it in run-time through some button. For this the *EXTI* and the interrupts come very handy.

**TODO: Read push button and change direction of the roulette**

https://www.pjrc.com/non-blocking-ws2812-led-library/


# More things to do

Well actually there could be more things done before we jump to main like setting the specified processor frequency, enabling FPU, debug access or the hardware watchdog but these are application specific and we'll cover them later. Now it's time to have a look at [memory map](chapter_5.html) which we'll need to access peripherals.

[Fields Medal]: https://en.wikipedia.org/wiki/Fields_Medal
[Late arrival source]: https://image.slidesharecdn.com/interruptsandthetimer-130715203042-phpapp01/95/interrupts-and-the-timer-7-638.jpg?cb=1373920529
[Tail chaining source]: https://image.slidesharecdn.com/interruptsandthetimer-130715203042-phpapp01/95/interrupts-and-the-timer-5-638.jpg?cb=1373920529
[Coresight source]: https://mcuoneclipse.files.wordpress.com/2016/10/coresight-debug.png