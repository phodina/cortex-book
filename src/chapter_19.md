 # Serial line

**TODO: Builder pattern**

## USART

As now we have a stable clock, we can configure the serial port - *Universal Asynchronous
Reciever/Transmitter* (UART). The USART offers many modes either synchronous or asynchronous, half or full-duplex e.g. It's even capable of emulating *Local Interconnect Network* (LIN) - can be found e.g. in car's door unit. However, our needs are quite humble - 19200 Bd, 8-bit frame, no parity, full duplex. The challenge will lie again in the API - tying the pin and clock source to the peripheral.

Our current GPIO pin setup allows us to take the Pin and configure it. We now extend it by the USART peripheral consuming the pins. Next step we need to validate the pins are allegeable - their alternate configuration allows us to configure it as pin for USART. After this check we configure appropriately the pin.

When we get a pin that does not support the alternate function for this peripheral the `init` method of the builder API will return error instead of ok struct.

The API for the USART will require us to create two structs *USARTBuilder* and *USART*.

**TODO: Mention Builder pattern and used typed structed**

```rust
struct USARTBuilder {
    
}

impl USARTBuilder {
    pub fn tx(self, pin: Pin) -> Self {
        
    }
    
    pub fn rx(self, pin: Pin) -> Self {
        
    }
    
    pub fn clk(self, pin: Pin) -> Self {
        
    }
    
    pub fn baudrate(self, baudrate: u32) -> Self {
        
    }
}
```

Lastly we'll implement the `core::fmt::Write` trait - to enable use of `write!` macro.

**TODO: Implement the config API as Builder**

### Protocols

**TODO: USART vs ITM**
**TODO: USART interrupts**
**TODO: Printing support though macros**

**TODO: Traits to support e.g. LIN, SmartCards**