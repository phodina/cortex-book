# Dockyard

## Crates, crates everywhere ...

As we near the end of this another long post it's time to restructure our code. By that I mean to split our crate into three: 

- **Cortex specific** (Cortex)

> Contains common functionality to hardware - currently reset handler and *Semihosting*

- **Hardware Abstraction Layer** (HAL)

> Definition and implementation of peripherals and their base addresses for various chips

- **Application specific** 

> Your application starting in main

Over the course of the next chapters you'll hopefully see the benefits in better reusability and maintenance of the code.

We'll take advantage of Cargo's feature called *Workspaces* where we can have our root directory containing single `Cargo.toml` and listing different crates - *members*:

```toml
[workspace]
members = ["chapter2","chapter3","chapter4","cortex","hal"]
```

Crates `chapter2` and `chapter3` remain unmodified and have no external dependencies. Crate `chapter4` is the first crate that approaches the new architecture by having dependencies on crates `cortex` and `hal`.

### Chapter4 crate

We move the `sys_request.rs` file to `cortex` crate . Then we modify the `main.rs`:

```rust
// chapter4/src/main.rs

#![no_std]
#![no_main]

extern crate cortex;
extern crate hal;

#[no_mangle]
pub fn main () -> ! {

    loop{}
}
```

**TODO: Check how to remove the `no_main` **

~~Thank to the decision to make `reset_handler` a never ending function we are now free to remove the `no_main` crate attribute as the `main` function signature matches to the standard one.~~

We make several changes to our `Cargo.toml`:

```toml
# chapter4/Cargo.toml
[dependencies]
cortex = {path = "../cortex", version = "0.1.0" }
hal = {path = "../hal", version = "0.1.0" }
```

So we moved dependency on `r0` crate to our `cortex` crate and added dependency on the newly created crates.

### Cortex crate

The crate will now depend on the `r0` crate and be built into Rust library. You can read more about various types of libraries in the [Linkage] chapter of Rust book.

```toml
# cortex/Cargo.toml
...
[lib]
name = "cortex"
crate-type = ["rlib"]

[dependencies]
r0 = "0.2.2"
```

The lib.rs file now contains the reset_handler, Vector table and panic implementation:

```rust
// cortex/src/lib.rs

//! This is the documentation for `cortex`
//! This crate contains the initialization code for the Cortex-M4 microcontroller
#![no_std]
#![feature(asm)]

extern crate r0;

pub mod sys_request;
use core::panic::PanicInfo;

#[panic_handler]
#[no_mangle]
pub fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

/// Initializes the Rust enviroment
/// 
/// 
/// # Remarks
///
/// This is the first function that is called after the processor is released from reset. The /// Rust environment is not properly intialized until we call r0::zero_bss and r0::init_data 
/// functions.
///
/// #Safety
///
/// This function must never return otherwise it will cause undefined behaviour.
#[no_mangle]
pub extern "C" fn reset_handler() -> ! {
// The type, `u32`, indicates that the memory is 4-byte aligned
    extern "C" {
        static mut _sbss: u32;
        static mut _ebss: u32;

        static mut _sdata: u32;
        static mut _edata: u32;

        static _sidata: u32;
    }

    unsafe {
        // Zero the BSS section in RAM
        r0::zero_bss(&mut _sbss, &mut _ebss);
        // Copy variables in DATA section in FLASH to RAM
        r0::init_data(&mut _sdata, &mut _edata, &_sidata);
    }

    extern "Rust" {
        fn main () -> !;
    }

    unsafe { main(); }
    
    loop {}
}

#[link_section = ".vector_table.reset_vector"]
#[no_mangle]
pub static __RESET_VECTOR: unsafe extern "C" fn() -> ! = reset_handler;

#[link_section = ".vector_table.exceptions"]
#[no_mangle]
pub static __EXCEPTIONS: [extern "C" fn(); 14] = [default_handler; 14];

#[link_section = ".vector_table.interrupts"]
#[no_mangle]
pub static INTERRUPTS: [extern "C" fn(); 240] = [default_handler; 240];

extern "C" fn default_handler() {
  loop{}
}
```

The other file that is also part of this crate is the `sys_request.rs` no modification done there.

### HAL crate

The last `Cargo.toml` has inherits the dependency on `register` crate as we want to implement here all the API related to the peripherals:

```toml
# hal/Cargo.toml
...
[lib]
name = "hal"
crate-type = ["rlib"]

[dependencies]
register = "0.2.0"
```

The `lib.rs` is pretty simple:

```rust
// hal/src/lib.rs

!#[no_std]

#[macro_use]
extern crate register;
pub mod gpio;
```

As you might have guessed the `gpio.rs` is part of this crate.

### Crate documentation

We can ask cargo to generate and open documentation on any crate our project depends on:

```txt
cargo doc -p cortex --no-deps --open
```

- `-p cortex` is a short for `--package cortex`, specifies that we want documentation for it
- `--no-deps` disables recursive documentation for all dependencies of `cortex`. We might want to disable that when dealing with crates that depend on other crates as generating  docs for them might take too much time.
- `--open` opens generated documentation in a browser window.

## Putting it together

**TODO: Highlight the external crates hal and cortex and the verbose flag**

Each time `rustc` sees line `extern crate NAME`; it knows where to find that compiled crate on disk. The `.rlib` contains the compiled code that is statically linked into the final executable. The `rlib` also contains type information, so Rust can check that the library features we're using in our code actually exists in the crate, that we're using them correctly. Besides that it contains a copy of crate's public inline functions, generics and macros - features that can't be fully compiled to machine code until Rust sees how we use them.

Rust never compiles modules separately, even if they're in separate files. When we build Rust crate we're recompiling all of its modules.

Functions from different crates are never inlined implicitly unless they are generics or they are explicitly marked by attribute `#[inline]`.

```shell
Running `rustc --edition=2018 --crate-name chapter4 chapter4/src/main.rs --crate-type bin --emit=dep-info,link -C debuginfo=2 -C metadata=74e0b433da62f8e2 -C extra-filename=-74e0b433da62f8e2 --out-dir /home/cylon2p0/Guides/cortex_guide/cortex/target/thumbv7em-none-eabi/debug/deps --target thumbv7em-none-eabi -C incremental=/home/cylon2p0/Guides/cortex_guide/cortex/target/thumbv7em-none-eabi/debug/incremental -L dependency=/home/cylon2p0/Guides/cortex_guide/cortex/target/thumbv7em-none-eabi/debug/deps -L dependency=/home/cylon2p0/Guides/cortex_guide/cortex/target/debug/deps --extern cortex=/home/cylon2p0/Guides/cortex_guide/cortex/target/thumbv7em-none-eabi/debug/deps/libcortex-dde9c79edb0e063f.rlib --extern hal=/home/cylon2p0/Guides/cortex_guide/cortex/target/thumbv7em-none-eabi/debug/deps/libhal-b33d9306b5273e70.rlib -C link-arg=-Tchapter2/layout.ld -C linker=lld-link -Z linker-flavor=ld.lld --sysroot /home/cylon2p0/Guides/cortex_guide/cortex/target/sysroot`
```



# Summary

This post showed us how to access memory mapped peripherals and how to create abstractions of the peripherals. Now we're armed to all the essential knowledge to [toggle LED](chapter_6.html) and how to improve our *GPIO API*.