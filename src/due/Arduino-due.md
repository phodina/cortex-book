## Arduino DUE
I'm not a big fan of Arduino boards and environment as they abstract a lot of the hardware to keep things simple and get you started on your project as soon as possible. But they are immensely popular and they is a lot of hardware to be reused. Though we won't cover Arduino UNO and others based on AVR chips (ATMEGA). We'll give some attention to mostly unknown board Arduino Due which runs ARM Cortex-M3 - now mostly obsolete.

### Programming
To program the board we'll need to install a BOSSA - a flash programming utility for Atmel's SAM microcontrollers.
We can get it from https://github.com/shumatech/BOSSA.
We create a new udev rule:
```ATTRS{idVendor}=="2341", ATTRS{idProduct}=="003d", MODE="0666"```

Then we just run bossac -e -U=1 -p <PORT>

To flash:
bossac --write --verify --boot -R blink.bin

const PB_PIO_ENABLE       : *mut u32 = 0x400E1000 as *mut u32;

As I explained before, I'm using an Arduino Due board for development. The Due uses Atmel's SAM3X8E microcontroller. Atmel provides a very thorough data sheet, which I'm going to refer to throughout the rest of this article. 

http://www.arduino.cc/en/Main/ArduinoBoardDue
http://www.atmel.com/devices/SAM3X8E.aspx
http://www.atmel.com/Images/Atmel-11057-32-bit-Cortex-M3-Microcontroller-SAM3X-SAM3A_Datasheet.pdf

Luckily, there's a better approach. Since the registers of a controller are located next to each other, we can use a struct to access them: 

pub struct Pio {
	pub pio_enable : u32,
	pub pio_disable: u32,
	pub pio_status : u32,


pub const PIO_A: *mut Pio = 0x400E0E00 as *mut Pio;

 And finally, let's be thorough and define constants to access all of the up to 32 pins per controller:

pub const P0 : u32 = 0x00000001;

const TIMER_MODE_REGISTER : *mut   u32 = 0x400E1A30 as *mut   u32;
const TIMER_VALUE_REGISTER: *const u32 = 0x400E1A38 as *const u32;