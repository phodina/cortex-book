 # Heirloom

## Async I/O

In the previous two posts we experimented with two types I/O access.

- Synchronous
- Asynchronous

One advantage of the *synchronous* approach is the easy implementation. We simply read the register again and again until it changes, so it's also sometime called polling. The disadvantage is clear, we have to wait unit the polling succeeds.

> Its good to implement some timeout for polling so your application doesn't hang indefinitely.

On the other hand the *asynchronous* approach would let's us do the following as Jorge Aparicio lists in his post [brave new API]:

- callback model ("run function when data is available")
- futures model ("function will be later completed, we can poll to check")
- generators model ("yield instead of blocking, when no more progress can be made")

Therefore for the synchronous, futures and generators we can implement the returning value of  type Result<Ok, Error> where Error is enumeration and can be either WouldBlock or any error associate with the peripheral. The WouldBlock would mean:

- In a blocking API, `WouldBlock` means try again i.e. busy wait (or maybe do something more elaborated if you have a threading system).
- In a futures based API, `WouldBlock` means `Async::NotReady`
- In a generator based API, `WouldBlock` means `yield`

We can implement a blocking API on top of the `svd2rust` API; we can implement a futures based version of the API on top of the `svd2rust` API; and we can implement a generator based version of the API on top of the `svd2rust` API. The implementations of those three flavors of the same API will look very similar. The question is: can we implement some intermediate API to avoid *writing register manipulation* code in those three implementations? The answer is the [`nb`](https://docs.rs/nb/0.1.1/nb/) crate.

I pulled a solution out of the Tokio book, or maybe it’s actually from the *nix book. The solution I chose was to implement the intermediate API as a non blocking API that returns a `WouldBlock` error variant to signal that the operation can *not* be completed right now. This non fatal error variant has a different meaning depending on the (a)synchronous model is used in:

- In a blocking API, `WouldBlock` means try again i.e. busy wait (or maybe do something more elaborated if you have a threading system).
- In a futures based API, `WouldBlock` means `Async::NotReady`
- In a generator based API, `WouldBlock` means `yield`

