# The time is up



**TODO: RCC,Clock State machine and Timers**

## Serial

In order to connect the serial pins of the STM32F407 to our computer, we will need some sort of USB serial interface. The embedded ST-Link is capable besides programming and debugging the chip to also facilitate the serial to USB adapter.

When the STM32F407 boots, it runs off of an internal reference clock *High speed internal* (HSI). Even though it is more than fast enough for our needs 16 MHz the main issue is the clock stability. In order to connect to another device over a serial interface, we need a clock error below about 3%.

Fortunately, the STM32F4 Discovery is equipped with an accurate 8 MHz crystal oscillator.  We will use this crystal as a reference to establish a very stable input frequency for the MCU.

We already covered the RCC peripheral which watches over the clock system. Its job is to control which clock source - either very accurate external crystal or a less-accurate internal reference - is used, and how that reference is multiplied and divided to produce the  clock speed for the core, bus and peripherals.

So the microcontroller runs off HSI after reset. Our goal will be to switch the clock source and run off the HSE and feed the clock into *Phased Locked Loop* (PLL). The PLL is then able to create from the 8 MHz coming from HSE output clock up to 186 MHz. The How PLL works is outside the scope of this post, but I recommend to read some articles as its present in every modern digital electronics running at speed over several tens of MHz.

We'll leverage *Rust's* type type system to ensure that our transitions between these sources are safe and correct.

The idea is to represent the Clock system as state machine where transitions between clock sources represent small modification to the RCC - mostly only a few bits change in a register. Nevertheless, the set of valid modifications changes with the mode and any invalid modification might cause the microcontroller itself to fail in unexpected way - the CPU rely on a stable clock. As with the GPIO API we'll  build a safe and usable API around RCC.

```rust
pub struct Hsi {
    rcc: &'static mut Rcc
}

pub struct Hse {
    rcc: &'static mut Rcc
}

pub struct Pll {
    rcc: &'static mut Rcc
}
```

Each state of in our state machine will only have methods implemented that allow only the changes to the RCC that are safe in this state.

Now, for each struct, we will need to define the functions that transition the clock to the next state. We will start with the HSI to HSE transition. For this, we need to enable the crystal oscillator, and then switch our output to it. This second operation will also consume the Hsi instance, returning an Hse instance representing the transition. We must must wait for our changes to take effect - read the status register in a loop until the RCC reports that it is in the expected state.

```rust
// TODO: Return options due to timeout
impl Hsi {
    pub fn use_external(self) -> Hse {
        
    }
    
    pub fn use_pll(self) -> Pll {
        
    }
}

impl Hse {
    pub fn use_internal(self) -> Hsi {
        
    }
    
    pub fn use_pll(self) -> Pll {
        
    }
}

impl Pll {
    pub fn use_internal(self) -> Hsi {
        
    }
    
    pub fn use_external(self) -> Hse {
        
    }
}
```



## Tread carefully

In last post I discussed the issue of a race condition that might happen during read/modify/write process (it would be pain later when we enable interrupts and run multiple tasks). Current approach would mean that another task/interrupt could have an impact on how our pin behaves.

The solution to this problem was to use the *bit-banding* - solution on the hardware level (on the bus). So we could theoretically define the structure for GPIO using *bit-banding* as:

```rust
#[repr(C,packed)]
struct GpioBB {
// Mode
pub mode: [u32; 32],
// Output type
otype: [u32; 32],
// Output speed
ospeed: [u32; 32],
// Pull-up/pull-down
pupd: [u32; 32],
// Input data
id: [u32; 32],
// Output data
od: [u32; 32],
// Bit set/reset
bsr: [u32; 32],
// Configuration lock register
lck: [u32; 32],
// Alternate function low
afl: [u32; 32],
// Alternate function high 
afh: [u32; 32],
}
```

So what have we done now? Basically we created two pointers - `Gpio` and `GpioBB`  that read and write to the same addresses. Does it sound like race condition, right? To tackle this issue we'll have to ask ourselves what else can modify the registers - DMA. We'll come to this issue later as it's common to every peripheral and focus now only on the API for the GPIO.

**TODO: Create an array of pins that will be borrowable**

**TODO: Make union of Peripheral and PeripheralBB. Then borrow this struct returning Option**

## Heartbeat



**TODO: Timer channel as PWM output**

## Board support package

There are many constraints in embedded world. One of them would be the connections between peripherals in the microcontroller to various devices. As you fabricate the board and pick & place the components its often for good (you normally don't rewire the connections).

In the beginning of this chapter we had to lookup the LEDs connections in the schematics. This is again tedious and error prone method. Familiar question, could we make the API in such a way that we don't need to know the connections and configurations? The answer is *Board support package*. 

```rust
let gpiod = p.GPIOD.split(&mut rcc.ahb);
let mut leds = Leds::new(gpiod);
for led in leds.iter_mut() {
    led.on();
    }
```

Using the BSP the user doesn't specify any pin connections in the code.

**TODO: Refactor**

## Roulette

**TODO: Implement iterators**

So we have turned on and off one LED. But we have 4 LEDs on the board. Let's put them to good use and create a roulette.

```rust
 let leds = [red_led(), green_led(), blue_led(), yellow_led()];

loop {
  leds.iter()
  .zip(leds.iter().cycle().skip(1))
  .for_each(|(current, next)| {
    next.on();
    current.off();
    led::delay(500000);
  });
}
```

Wait a minute. I though we want to write a high performance code. What about this mess of iterators and closures?

All of these abstractions will compile down to disassembly:
**TODO: dump of roulette function**

In the listing we see only sequence of bit set and clear operations on an I/O port; the compiler has
broken down all the abstractions to perform the minimal amount of work which gets the job
done!



[ *DWT* peripheral ]: https://developer.arm.com/docs/ddi0337/e/system-debug/dwt/summary-and-description-of-the-dwt-registers#BABEHDCI
[book]: https://doc.rust-lang.org/1.8.0/book/references-and-borrowing.html
[Singleton Pattern]: https://en.wikipedia.org/wiki/Singleton_pattern