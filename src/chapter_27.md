 Waiting for the rescue

# Rewrite

- Explain FMC & SDRAM
- Integration of legacy C code
- Example - Read/write
- Explain HEAP
- Example - heap (String, Box)
- VFP (floating point)
- Explain Testing
- Example - unit, component, system
- Example - branch coverage

**TLDR;**

**TODO: Switch to STM32F7**

**TODO: Debug Monitor to unit test**

**TODO: Graphics, 2D and DOOM, next post window system and GUI in Rust**

## Increasing RAM

As the memory on chip is limited due to the size of die, manufacturing process and cost, we'll now focus on increasing the amount of RAM we have by attaching an external one. Back in chapter 4 we talked about bus matrix and memory map. If we have a look again you can see that there is a special address range - external memories - specified by ARM. 

*Flexible memory controller* (FMC) is a special peripheral which generates all the neccessary signal in order to read/ write to the memory.

## Types of memories

## Inside memory controller



## Initialization of SDRAM

Initialization of SDRAM controller is done by configuration of several registers. The most important  are *SDRAM Control Register* (SDCR) and *SDRAM Timing Register* (SDTR) registers. The FMC peripheral has two banks allowing us to connect two memories at once. Each bank has its own SDCR/SDTR registers. In case of *STM32F429* we have only one 8 MB SDRAM which is connected to bank 2. Though we want to use only bank 2 we'll have to configure some registers also in bank 1.

```rust
const SDRAM_BASE: usize = 0xD0000000;
const SDRAM_SIZE: u32 = 0x00800000;
```

Let's inspect registers:

| Struct field name | Possible values           | Comments                                              |
| ----------------- | ------------------------- | ----------------------------------------------------- |
| SDBank            | BANK1,BANK2               |                                                       |
| ColumnBitsNumber  | 8-11 bits                 |                                                       |
| RowBitsNumber     | 11-13 bits                |                                                       |
| MemoryDataWidth   | 8,16,32 bits              |                                                       |
| CASLatency        | 1,2,3                     |                                                       |
| WriteProtection   | ENABLE,DISABLE            |                                                       |
| SDClockPeriod     | DISABLE, PERIOD2, PERIOD3 | If system clock 180Mhz then CLK speeds 90Mhz or 60Mhz |
| ReadBurst         | ENABLE,DISABLE            |                                                       |
| ReadPipeDelay     | DELAY_0, DELAY_1, DELAY_2 |                                                       |

and

| LoadToActiveDelay    | 1-16 cycles | TMRD |
| -------------------- | ----------- | ---- |
| ExitSlefRedreshDelay | 1-16 cycles | TXSR |
| SelfRefreshTime      | 1-16 cycles | TRAS |
| RowCycleDelay        | 1-16 cycles | TRC  |
| WriteRecoveryTime    | 1-16 cycles | TWR  |
| RPDelay              | 1-16 cycles | TRCD |
| RCDelay              | 1-16 cycles | TRC  |

We'll define several macros to help us with the configuration, so that we can just copy the values from datasheet of the memory.

```rust
#[doc = "Load Mode Register to Active"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 0 };
}

#[doc = "Exit Self-refresh delay"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 4 };
}

#[doc = "Self refresh time"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 8 };
}

#[doc = "Row cycle delay"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 12 };
}

#[doc = "Recovery delay"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 16 };
}

#[doc = "Row precharge delay"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 20 };
}

#[doc = "Row to column delay"]
macro_rules! tmrd {
  ( $x:expr ) => { $x << 24 };
}
```

## Tests

**TODO: Figure out how to run them**

Unit now we've developed code, built and flashed it to the board. Either through attached debugger, toggling LED or dumped output we checked that code performed as intended. However, there is also another way.

Rust has built-in test framework capable of running tests without any hassle.
The test runner looks for functions with `#[test]` attribute - inside we put code under test and assert the results. Then `cargo test` collects all the tests, runs them and displays results.

The tests can be divided into three groups:

- Unit tests
- Integration tests
- System tests

> Cargo itself also runs tests found in documentation which are there to explain the API and ensure code change doesn't break it.

Let's run the tests:

```shell
cargo test


## Unit tests
Unit test can be thought of as the smallest testable part of an application. In our case it can be the GPIO peripheral. We can test the methods that configure the pins - e.g. when pin is locked its configuration mustn't be modified.

## Intergration tests

## Summary

With the external memory initialized and tested we can now proceed to LTDC and DMA2D to control graphical displays and display some bitmaps.
```