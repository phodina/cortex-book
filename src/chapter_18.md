# Meet Gutenberg

## Meet Gutenberg

Until now we chose to manually write the definitions of the peripherals and their registers as we only support one device *STM32F407*. We've seen how tedious and error-prone it could be. However, we chose not to utilize such tools because of the reasons described below.

- It was quick and easy to get started with code for a new peripheral. This argument was especially important when this series started out, because we still had no clue of how the API would look like.
- It was an advantage to depend on as few third party tools as possible.
- We wanted to keep the naming convention of our API as similar to *Reference Manua*l as possible.
- We could focus on writing API for smaller parts of each module separately and leave the uneeded stuff.

Therefore now we will focus more attention to tools that allow us to generate register definitions.

### Tailoring the code

(Most) humans are pretty lazy so why should we be any different. No I'm not talking about closing the computer and going to watch the TV. On contrary we'll (well me) write a parser that will read the SVD files containing the definitions of registers of the microprocessor and generate definition for Rust.

`Tailor` is command line based tool that eats on input SVD file and spits out register definition files independent of the language. Why language independent? We'll it's a parser and generator under the hood. All the knowledge of about the language syntax is hard coded in templates - outside the tool itself. That way the tool remains quite simple - it's up to the programmer to write the template correctly.