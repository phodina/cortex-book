# Situation room and pulse

## Automatization

So the microcontroller now boots up and we initialized the *Rust* environment - now we can run *Rust* code. There's saying: *Take one step futher make two back*. To minimize our effort in the future we'll first glide over several topics.

## Build script

All the source files written in *Rust* are managed by *Cargo*, therefore any modification of the code will trigger rebuild of that file. However, not all the files in the project are written in *Rust*. One such glowing example is the linker script. You could argue that this file rarely changes and I'd agree but it allows us to have a look on `build.rs` script.

 First, we need to tell *Cargo* to expect such a file by modifing `Cargo.toml`:

```toml
# Cargo.toml
[package]
...
build = "build.rs"
```

So now *Cargo* will run `build.rs` each time we build our application. The contents of `build.rs` are following:

```rust
pub fn main() {
	println!("cargo:rerun-if-changed=layout.ld");
}
```

The beauty of `build.rs` is that you can pass arguments on standard output and they get passed by *Cargo* to `rustc` or in this case *Cargo* checks whether the file has been modified.

If you want to learn more about `build.rs` you can check [The Cargo book]. In later chapters we'll expand on this file by compiling legacy code written in *C/C++*.

## Automate GDB

Ok the tasks for debugging and flashing are automated. Nevertheless we still need to write the commands in the gdb console manually. If we run `arm-none-eabi-gdb --help` we get the follwing output (trimmed):

```shell
$ arm-none-eabi-gdb --help
This is the GNU debugger.  Usage:

    gdb [options] [executable-file [core-file or process-id]]
    gdb [options] --args executable-file [inferior-arguments ...]

...

Initial commands and command files:

  --command=FILE, -x Execute GDB commands from FILE.
  --init-command=FILE, -ix
                     Like -x but execute commands before loading inferior.
...
```

So we can pass file containing the commands to execute. Now we put the commands into three files - `flash.gdb`, `debug.gdb` and `connect.gdb`:

```
# flash.gdb
target remote localhost:3333
load
continue
quit
```

> Connect, load executable, run it and exit

```
# debug.gdb
target remote localhost:3333
load
tbreak main
continue
```

> Connect, load executable, set temporary breakpoint to main and continue

```
# connect.gdb
target remote localhost:3333
```

> Connect to running target

> We could create .gdbinit where we could put the command to connect to gdb-server. However, `.gdbinit` requires user to allow it in global configuration file as security measure.

> We could also connect over telnet to OpenOCD on port 4444 and flash the binary that way.

## Cargo make

To flash the executable to the board we need to first run the `openocd`, then `arm-none-eabi-gdb` and inside execute several commands. This is quite tedious, isn't it? Can't we just write a `Makefile` with targets for flashing and debugging? We can do even better with `Cargo-make` - *Rust* task runner and build tool. 

First we need to install `Cargo-make`:

```shell
$ cargo install cargo-make
```

Then we crate the `Makefile.toml`:

```toml
[tasks.openocd]
script = [
 "openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg"
]

[tasks.flash]
script = [
 "arm-none-eabi-gdb -ix flash.gdb ${@}"
]

[tasks.debug]
script = [
 "arm-none-eabi-gdb -ix debug.gdb ${@}"
]

[tasks.connect]
script = [
 "arm-none-eabi-gdb -ix connect.gdb ${@}"
]
```

## Semihosting

With all the automation out of the way let's finish this post with *Semihosting*. The definition can be found out [ARM's website]:

> Semihosting is a mechanism that enables code running on an ARM target to communicate     and use the Input/Output facilities on a host computer that is running a debugger.

The examples of these facilities include keyboard input, screen output, and disk I/O. In the world of *C/C++* we use functionality of *C* library, such as `printf()` and `scanf()` -  use the screen and keyboard of the host and attach it to the target system.

*Semihosting* is implemented by a set of defined software instructions, e.g. *SVC*s, that generate exceptions from program control. The application on the target invokes the appropriate *Semihosting* call and the debug agent traps the exception and executes the appropriate handler. As a benefit of this approach we'll not need anything else beside several lines of code and modification of the *GDB* script - no extra hardware required.

**TODO: Semihosting connection figure**



### Semihosting steps

Let's look at the steps it takes to execute the required functionality:

1. The target executes a **breakpoint** instruction with a special tag.
2. The debugging-software on the host is **notified** of the breakpoint.
3. Information inside the **first two registers** indicates which procedure should be called and points to a structure with arguments.
4. The debugger uses its memory-reading capabilities to **retrieve the arguments** and passes these on to the host's procedure.
5. The target's CPU is unhalted by the debugger and **execution continues**.

In the official documentation we can find a note regarding *ARMv6-M* or *ARMv7-M* microcontrollers. To facilitate semihosting they use the `BKPT` instruction as opposed to *ARM* processors prior to *ARMv7* the use of `SVC` instruction - meaning it won't interfere with system calls of Operating system running on the target. 

The argument of the `BKPT` instruction must be `0xAB` which is acknowledged as a request for *Semihosting*. The type of requested operation is passed in `R0`. All other parameters are passed in a block that is pointed to by `R1`. 

The documentation lists the available semihosting operation numbers passed in `R0` as follows:

- `0x00-0x31`

  Used by *ARM*.

- `0x32-0xFF`

  Reservedfor future use by *ARM*.

- `0x100-0x1FF`

  Reserved for user applications. These are not used by *ARM*. If you are writing your own *SVC* operations, however, you areadvised to use a different *SVC* number rather than using the semihosted *SVC* number and these operation type numbers.

- `0x200-0xFFFFFFFF`

  Undefined and currently unused. It is recommendedthat you do not use these.

The most promising operation to us is *SYS_WRITE* (0x05) which will pass file descriptor, buffer and its length to the debugger and request a write of that buffer into the specified file descriptor. In our case eihter standard or error output.

## Semihosting from Rust

First we'll try it manually. In the source code of main function we put the following line:

```rust
// main.rs
#![feature(asm)]

fn main () {
  ...
  asm!("bkpt 0xAB");
  ...
}
```

You can run assembly code from Rust given that you enable this crate attribute. I'll cover the syntax of the assembly macro in later post. Now we just connect with the debugger.

**TODO: Dump output of the debugger**

```

```

We can see that the debugger traps the exception.

Next we implement a general *Semihosting* function in a new file `sys_request.rs`:

```rust
// sys_request.rs
unsafe fn sys_request(num: usize, addr: *const ()) -> usize {
    let result: usize;

    asm!("mov r0,$1\n\t\
          mov r1,$2\n\t\
          bkpt 0xAB\n\t\
          mov $0,r0"
        : "=ri"(result)
        : "ri"(num), "ri"(addr)
        : "r0", "r1"
        : "volatile"
       );

    result
}
```

Parameter to `r1` is address of a structure containing the parameters for *SYS_WRITE* operation:

```rust
// sys_request.rs
#[repr(C)]
struct SysWrite {
    fd: usize,
    addr: *const u8,
    len: usize,
}
```

When put together it forms our printing function:

```rust
// sys_request.rs
const SYS_WRITE: usize = 0x05;
pub const FD_STDOUT: usize = 1;
pub const FD_STERR: usize = 2;

fn sys_write(fd: usize, data: &[u8]) -> usize {
    let args = SysWrite {
        fd: fd,
        addr: data.as_ptr(),
        len: data.len(),
    };

    unsafe { sys_request(SYS_WRITE, &args as *const SysWrite as *const ()) }
}
```

In `main.rs` we include our module and call the function:

```rust
// main.rs
#![feature(asm)]
mod sys_request;

#[no_mangle]
pub fn main () -> ! {
  ...
  sys_request::sys_write(sys_request::FD_STDOUT, b"I can see though I'm blind...\n");
  ...
}
```

As *Rust* stores the strings in *UTF-8* format we'll need to convert the string into C representation - ASCII format ending with `\0` delimiter. Why? Well we could pass the string in UTF-8 format but *OpenOCD* expects *ASCII* string.

> We could write the handler ourselves in Pyhton and source it in GDB - printing directly UTF-8.

However, we can still not use the full blown *String* as we haven't yet implemented the memory allocator. We'll overcome this restriction by using `str` and specifying lifetime to be ``static`.

> There is another common file descriptor hiding under number 3 - *Standard input* (STDIN). This would allow us to pass data to target. However, we would need to implement the data structure SysRead and function sys_read.

Before we build the code we'll need to do slight adjustment to the linker script:

```
/* layout.ld */
SECTIONS
{
    .text : {
    *(.text*)
     _srodata = .;
	*(.rodata*)
	*(.rodata)
    _erodata = ALIGN(4);
    } > FLASH = 0xFF
...
}
```

So we put the strings from `.rodata` into the FLASH. Thanks to the build script the change will trigger rebuild.

> I tried to put `.rodata` into seperate section but then there is some bug in gdb as it loads this section to different address in memory.
>
> More information can be found here [stackoverflow].

The `sys_write` call triggers the debugger:

```
 (gdb) p/x $pc
 (gdb) disassemble 0x80102e4,+2
 (gdb) p/x $r0
$1 = 0x5
(gdb) p/x $r1
$2 = 0x2002ffc8
(gdb) printf "%17s", 0x08010008
I can see though I'm blind...
```

As I love automation we could do the same stuff with just one additional command to the GDB.

```
(gdb) monitor arm semihosting enable
```

We get the string printed in *OpenOCD*:

```
I can see though I'm blind...
```

**TODO: https://doc.rust-lang.org/nightly/core/intrinsics/fn.init.html**

## Println! macro

Third chapter and we get string message in our console. Could we do any better? Sure we could. Let's add support for formatted output. The key here is of course the `print!` macro defined in *std::io* :

```rust
// std/macros.rs

#[macro_export]
#[stable(feature = "rust1", since = "1.0.0")]
#[allow_internal_unstable]
macro_rules! print {
    ($($arg:tt)*) => ($crate::io::_print(format_args!($($arg)*)));
}
```

The Rust's macro syntax has pretty steep learning curve. I'd like not to confuse you even further as there are several types of macros. We'll instead focus only on *Declarative Macros* (known as plain macros). They allow you to define one or more rules, that are similar to `match` syntax.

The macro itself expands into a code that calls the `_print` function in the `io` module. Notice also the `$crate` variable - ensures the macro works from outside the `std` crate. 

However, the rabbit hole goes even deeper. The `print!` macro contains another macro - `format_args!`. 

```rust
// std/macros.rs

#[stable(feature = "rust1", since = "1.0.0")]
#[rustc_doc_only_macro]
macro_rules! format_args {
  ($fmt:expr) => ({ /* compiler built-in */ });
  ($fmt:expr, $($args:tt)*) => ({ /* compiler built-in */ });
}
```

But let's get back to our implementation. We'll have just two rules:

- Invocation with a single parameter (e.g. `print!("Rust")`)
- Invocation with additional parameters (e.g. `print!("{} equals {} in binary")`, 3,11)

**TODO: Define rules**

To print our string we have to provide implementation of `_print` function. Though one that is definitely less complicated than the one in `std` as we just want to stream our characters over serial interface. Therefore we'll call the `print`  through *Semihosting* .

With the `print!` macro defined we can now define a close relative - `println!` - simply by appending a newline character (`\n`) to the format string and then invoking the rest of the `print!` macro. 

```rust
// sys_request.rs
use core::fmt;

struct Sys {}

impl fmt::Write for Sys {
    fn write_str(&mut self, s: &str) -> fmt::Result {

        sys_write(0, s.as_bytes());
        Ok(())
    }
}

macro_rules! sysprint {
    ($($arg:tt)*) => ($crate::sys_request::print(format_args!($($arg)*)));
}

macro_rules! sysprintln {
    () => (sysprint!("\n"));
    ($fmt:expr) => (sysprint!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (sysprint!(concat!($fmt, "\n"), $($arg)*));
}

fn print(args: fmt::Arguments) {
    use core::fmt::Write;
    let mut s = Sys{};
    s.write_fmt(args).unwrap();
}
```

Maybe you've notice the extra `{}` scope around the macro ( `=> ({…})` instead of `=> (…)`). As a result the `Write` trait is not silently imported in parent scope when we call `print` - we'll import it manually.

To use the macro we'll have to use crate attribute `#[macro_use]` in order to import the macro in our crate.

```rust
#[macro_use]
mod sys_request;

#[no_mangle]
pub fn main() -> ! {

    sysprintln!("Hello Rust{}", "!");

    loop {}
}
```

# Summary

Now we know little more about the *ARM Cortex-M4* architecture and with the booting and initilization code completed we can now run *Rust* code. Together with several scripts we automated most of the tasks and the code for *Semihosting* now allows to print text to console helping us develop code in next posts.

[Netburst]: https://en.wikipedia.org/wiki/NetBurst_(microarchitecture)
[wikipedia]: https://en.wikipedia.org/wiki/Instruction_pipelining#/media/File:Pipeline,_4_stage.svg
[Instruction set source]: https://images.anandtech.com/doci/8400/Screen%20Shot%202014-08-18%20at%206.04.32%20PM.png
[The Cargo book]: https://doc.rust-lang.org/cargo/reference/build-scripts.html
[ARM&#39;s website]: http://www.keil.com/support/man/docs/armcc/armcc_pge1358787046598.htm
[ stackoverflow ]: https://stackoverflow.com/questions/51911516/why-does-the-content-of-stm32-read-only-data-set-by-rust-differ-between-the-targ?noredirect=1#comment90799084_51911516