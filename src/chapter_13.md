 #  Counting ~~money~~ instructions

## Measuring clock cycles

Most of the *Cortex-M3*, *M4* and *M7* devices have implemented *Data Watchpoint and Trace* (DWT) unit which helps us count the number of execution steps. Beside that it contains 4 comparators that can be configured as hardware watchpoint.

The first comparator, DWT_COMP0, can also compare against the clock cycle counter, CYCCNT. The second comparator, DWT_COMP1, can also be used as a data comparator.

- clock cycles (CYCCNT) 
- folded instructions 
- Load Store Unit (LSU) operations 
- sleep cycles 
- CPI (all instruction cycles except for the first cycle) 
- interrupt overhead.

Before we can configure our *DWT* for measuring the executed clock cycles we need to enable tracing in *CoreDebug*. So we define the registers and bitfields:

```rust
// cortex/src/coredebug.rs

/// Core Debug
#[repr(C)]
pub struct CoreDebug {
	/// Debug Halting Control and Status (0xE000EDF0)
	pub dhcs: ReadWrite<u32, DHCS::Register>,
	/// Register Selector(0xE000EDF4)
	pub rs: ReadWrite<u32>,
	/// Register Data (0xE000EDF8)
	pub rd: ReadWrite<u32>,
	/// Monitor Control (0xE000EDFC)
	pub mc: ReadWrite<u32, MC::Register>
}

register_bitfields! [u32,
    ...
    MC [
        ...
        TRCENA OFFSET(24) NUMBITS(1) []
    ]
    ];
```

Next we need to instantiate it and implement the metod to enable tracing peripherals.

```rust
// cortex/src/coredebug.rs

impl CoreDebug {

    pub fn init () -> &'static mut CoreDebug {
        unsafe {&mut *(COREDEBUG_BASE as *mut CoreDebug)}
    }

    pub fn enable_trace(&self) {
        self.mc.modify(MC::TRCENA::SET);
    }
}
```

With the method to enable clocks for tracing peripherals enabled we can now focus on the [*DWT* peripheral] itself.

```rust
// cortex/src/dwt.rs

/// Data Watchpoint and Trace
#[repr(C)]
pub struct DWT {
	/// Control
	pub ctrl: ReadWrite<u32, CTRL::Register>,
	/// Cycle Count
...
}

register_bitfields! [u32,
    CTRL [
    	CYCCNTENA OFFSET(0) NUMBITS(1) [],
...
```

We again instantiate the peripheral and reset immedietaly the counter.

```rust
// cortex/src/dwt.rs

impl DWT {

	pub fn init () -> &'static mut DWT {
    	let dwt = unsafe {&mut *(DWT_BASE as *mut DWT)};
    	dwt.cyccnt.set(0x00000000);
    	dwt
	}

	pub fn enable_cycle_counter (&mut self) {
		self.ctrl.modify(CTRL::CYCCNTENA::SET);
	}

	/// NOTE: atomic read (no side effects)
	pub fn get_cycle_count(&self) -> u32 {
		self.cyccnt.get()
	}
}
```



**TODO: File an issue on github about read only bits in  readwrite register and macro to generate the bits on a pattern and bitoverlap**

**TODO: Write key Debug Key. `0xA05F`**

**TODO: Benchmarking the code**

- DEMCR register: <http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0337e/CEGHJDCF.html>

**TODO: Disassembly of the code**

### Debug

```
08000440 <main>:
 8000440:       b08a            sub     sp, #40 ; 0x28
 8000442:       f000 f9d9       bl      80007f8 <_ZN3hal3rcc3RCC4init17h0ff962fd6f0757adE>
 8000446:       9006            str     r0, [sp, #24]
 8000448:       e7ff            b.n     800044a <main+0xa>
 800044a:       9806            ldr     r0, [sp, #24]
 800044c:       9007            str     r0, [sp, #28]
 800044e:       9907            ldr     r1, [sp, #28]
 8000450:       f101 0010       add.w   r0, r1, #16
 8000454:       2108            movs    r1, #8
 8000456:       9105            str     r1, [sp, #20]
 8000458:       9a05            ldr     r2, [sp, #20]
 800045a:       f000 f939       bl      80006d0 <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17h4673f91b41fb8e12E>
 800045e:       e7ff            b.n     8000460 <main+0x20>
 8000460:       9807            ldr     r0, [sp, #28]
 8000462:       3010            adds    r0, #16
 8000464:       2108            movs    r1, #8
 8000466:       2200            movs    r2, #0
 8000468:       f000 f932       bl      80006d0 <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17h4673f91b41fb8e12E>
 800046c:       e7ff            b.n     800046e <main+0x2e>
 800046e:       9807            ldr     r0, [sp, #28]
 8000470:       3030            adds    r0, #48 ; 0x30
 8000472:       2108            movs    r1, #8
 8000474:       9104            str     r1, [sp, #16]
 8000476:       9a04            ldr     r2, [sp, #16]
 8000478:       f000 f8cc       bl      8000614 <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17h07fc3d58bbbcefb2E>
 800047c:       e7ff            b.n     800047e <main+0x3e>
 800047e:       f640 4000       movw    r0, #3072       ; 0xc00
 8000482:       f2c4 0002       movt    r0, #16386      ; 0x4002
 8000486:       9009            str     r0, [sp, #36]   ; 0x24
 8000488:       9809            ldr     r0, [sp, #36]   ; 0x24
 800048a:       f000 f9ae       bl      80007ea <_ZN3hal4gpio4GPIO4init17hc951aedf95ed649dE>
 800048e:       9003            str     r0, [sp, #12]
 8000490:       e7ff            b.n     8000492 <main+0x52>
 8000492:       9803            ldr     r0, [sp, #12]
 8000494:       9008            str     r0, [sp, #32]
 8000496:       9808            ldr     r0, [sp, #32]
 8000498:       f04f 4140       mov.w   r1, #3221225472 ; 0xc0000000
 800049c:       f04f 4280       mov.w   r2, #1073741824 ; 0x40000000
 80004a0:       f000 f8e7       bl      8000672 <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17h1239b9db139145f7E>
 80004a4:       e7ff            b.n     80004a6 <main+0x66>
 80004a6:       9808            ldr     r0, [sp, #32]
 80004a8:       3008            adds    r0, #8
 80004aa:       f04f 4140       mov.w   r1, #3221225472 ; 0xc0000000
 80004ae:       f04f 4200       mov.w   r2, #2147483648 ; 0x80000000
 80004b2:       f000 f93c       bl      800072e <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17h553e72feacfb1decE>
 80004b6:       e7ff            b.n     80004b8 <main+0x78>
 80004b8:       9808            ldr     r0, [sp, #32]
 80004ba:       3014            adds    r0, #20
 80004bc:       f44f 4100       mov.w   r1, #32768      ; 0x8000
 80004c0:       9102            str     r1, [sp, #8]
 80004c2:       9a02            ldr     r2, [sp, #8]
 80004c4:       f000 f962       bl      800078c <_ZN63_$LT$tock_registers..registers..ReadWrite$LT$T$C$$u20$R$GT$$GT$6modify17hbe377c6b3187517bE>

```

### Release

```
08000400 <main>:
 8000400:       f000 f82a       bl      8000458 <_ZN3hal3rcc3RCC4init17h53f618b643618eceE>
 8000404:       6901            ldr     r1, [r0, #16]
 8000406:       f041 0108       orr.w   r1, r1, #8
 800040a:       6101            str     r1, [r0, #16]
 800040c:       6901            ldr     r1, [r0, #16]
 800040e:       f021 0108       bic.w   r1, r1, #8
 8000412:       6101            str     r1, [r0, #16]
 8000414:       6b01            ldr     r1, [r0, #48]   ; 0x30
 8000416:       f041 0108       orr.w   r1, r1, #8
 800041a:       6301            str     r1, [r0, #48]   ; 0x30
 800041c:       f640 4000       movw    r0, #3072       ; 0xc00
 8000420:       f2c4 0002       movt    r0, #16386      ; 0x4002
 8000424:       f000 f817       bl      8000456 <_ZN3hal4gpio4GPIO4init17h29ee90ab3b126c19E>
 8000428:       6801            ldr     r1, [r0, #0]
 800042a:       2201            movs    r2, #1
 800042c:       f362 719f       bfi     r1, r2, #30, #2
 8000430:       6001            str     r1, [r0, #0]
 8000432:       6881            ldr     r1, [r0, #8]
 8000434:       2202            movs    r2, #2
 8000436:       f362 719f       bfi     r1, r2, #30, #2
 800043a:       6081            str     r1, [r0, #8]
 800043c:       6941            ldr     r1, [r0, #20]
 800043e:       221d            movs    r2, #29
 8000440:       f441 4100       orr.w   r1, r1, #32768  ; 0x8000
 8000444:       6141            str     r1, [r0, #20]
```

**TODO: Release code for comparison**

Examining the binaries while testing this interface, everything compiles
down to the optimal inclined bit twiddling instructions--in other words, there is
zero runtime cost.

**TODO: Measure the number of instruction it takes**

## Debug Trace Output? SWO!

As the standard text and command line interface to my target boards I’m using a normal UART/SCI. However, on many boards the UART’s are used by the application.

There is [semihosting](https://mcuoneclipse.com/2016/08/06/semihosting-again-with-nxp-kinetis-sdk-v2-0/), but this is very slow, depends on the debugger and toolchain/library used plus is a wast of FLASH and RAM so I don’t recommend using semihosting at all.

There is [USB CDC](https://mcuoneclipse.com/2014/10/25/usb-cdc-with-the-frdm-k64f-finally/), but this requires a USB connector, a USB stack and a microcontroller capable of USB. Not applicable in all cases.

There is [Segger RTT](https://mcuoneclipse.com/2015/07/07/using-segger-real-time-terminal-rtt-with-eclipse/) which is small, fast and best of all does not need any special pins. But works only with Segger debugging probes.

**TODO: Modify the OpenOCD**

```
source [find interface/stlink-v2-1.cfg ]

transport select hla_swd

source [find ./stm32f3x.cfg ]

reset_config srst_only

tpiu config external uart off 64000000 2000000 
```



## Single Wire Output (SWO)

We'll now explore hardware feature of ARM Cortex-M called *Single Wire Output* (SWO). It allows to send out data (e.g. strings) over up to 32 different stimulus ports, over a single wire. Another benefit is the speed much higher than UART or semi-hosting.

The common SWO usages are:

- Sending debug messages as strings
- Recording interrupt/function entry/exit
- Event notification
- Variable or memory cell change over time

We're interested in the first one.

SWO is part of the ARM CoreSight Debug block which usually is part of Cortex-M3, M4 and M7. It can use UART or Manchester coding though we'll use the former.

![CoreSight](/home/cylon2p0/Guides/cortex_guide/cortex_book/img/coresight-debug.png)
Figure [Coresight source]

**TODO: breakpoints, watchpoints, `bkpt`**

Let's now locate the SWO pin on STM32:

**TODO: Figure of SWO on STM32**

We can use the embedded ST-Link to read the output on SWO.

However before we can write any message to the ITM port we need to configure it first although it can be configured also through hardware over SWD/JTAG (wtih the help of tools like Segger RTT viewer).

### ITM setup

Before we try to initialize the ITM we need to enable the bit *TRCENA* in Core debug peripheral which is connected directly to the AHB.

> Registers in Core debug are not reset by a system reset. They are reset only by **power on reset**
> The SWO trace output clock is derived from the CPU clock, so our function will need the clock speed and the SWO port number for the initialization.

```rust
// swo.rs
fn swo_enable(&self, swo_speed: u32){
    let prescaler: u32 = (CLOCK_SPEED / swo_speed) - 1;

    let debugmcu = super::debugmcu::DBGMCU::init();
    debugmcu.c.modify(super::debugmcu::C::TRACE_IOEN::SET);

    let tpiu = super::tpiu::TPIU::init();
    tpiu.spp.modify(super::tpiu::SPP::NRZ::SET);
    tpiu.asps.set(prescaler);
    tpiu.ffc.set(0x102);

    // TODO: Key as bitfield
    self.la.set(0xC5ACCE55);
    // Trace bus ID for TPIU, Enable events from DWT, Enable sync packets, Main enable for ITM
    self.tc.modify(TC::TRACEBUSID::SET + TC::DWTENA::SET + TC::SYNCENA::SET + TC::ITMENA::SET);
    self.tp.set(0x0000000F);
    // Enable all stimulus ports
    // TODO: Ports as bitfield
    self.te.set(0xFFFFFFFF);

    let dwt = super::dwt::DWT::init();
    // Formatter and Flush Control Register
    dwt.ctrl.set(0x40011a01);

    let etm = super::etm::ETM::init();
    etm.c.set(0x400);
}
```

### Printing debug message

Now we have the *ITM* initialized and it's time to write function that will print the debug message:

```rust
pub fn write_val(&self, port: usize, data: u8) {
        
    while self.port.get() == 0 {}
    self.port.set(data);
}
```

To make it easier to print a string:

```rust
 pub fn print(&self, port: usize, data: &str) {

     for d in data.bytes() {
         while self.port.get() == 0 {}
         self.port.set(d);
     }
}
```

**TODO: Print debug message over ITM**

### Capturing SWO output

**TODO: STM32 connection to STLink and serial viewer**

**TODO:  Dump registers**

https://blog.feabhas.com/2013/02/developing-a-generic-hard-fault-handler-for-arm-cortex-m3cortex-m4/

## Determine reset cause

With the stack pointer setup and RAM initialized we can take a small detour before calling our main function and have a look on the cause of the reset. The cause is stored in *Reset and Clock Control* (RCC) peripheral in *Control/status register* (CSR). If we have a look at STM32 in the reference manual we'll see the following reset causes:

**TODO: Add address of RCC/CSR register + defintions**

- Low-power

- Window watchdog

- Independent watchdog

- Software reset

- POR/PDR

- NRST

  The cause of reset is vital if we put the microcontroller into deep sleep (stop mode in case of STM32) where all the content of RAM is lost and the microcontroller starts to execute reset handler again upon receiving wake-up signal. Without this check we'll execute the same code as we did the first time we applied power to the microcontroller.

```rust
// main.rs

const RCC_BASE: u32 = 0x40023800;
const RCC_CSR: *mut u32 = (RCC_BASE + 0x74) as *mut u32;

#[no_mangle]
pub extern "C" fn reset_handler() -> ! {

    ...

    reset_cause();

    main();
    loop {}
}

fn reset_cause() {
    let rcc_csr = core::ptr::read_volatile(RCC_CSR);
    // Low-power
    if (rcc_csr & 1 << 31) != 0 {

    }
    // Window watchdog
    else if (rcc_csr & 1 << 30) != 0 {
        
    }
    // Independent watchdog
    else if (rcc_csr & 1 << 29) != 0 {
        
    }
    // Software
    else if (rcc_csr & 1 << 28) != 0 {
        
    }
    // POR/PDR
    else if (rcc_csr & 1 << 27) != 0 {
        
    }
    // NRST pin
    else if (rcc_csr & 1 << 26) != 0 {
        
    }
    // BOR
    else if (rcc_csr & 1 << 26) != 0 {
        
    }
    else {
        
    }

    // Clear reset flags
    core::ptr::write_volatile(1<<24);
}
```

# Summary 

So we now know how to play with LED and read push button. The TIMER and DWT allowed us to select frequency of blinkning. In the next installment we'll use interrupts to act on events. During startup we display boot information.