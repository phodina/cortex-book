# Lighthouse

**TLDR**;

Finally a post where there is more code than theory. We'll turn on the LED, toggle it, read the push button turning the board into a roulette with reverse activation. To slow down the toggling we use `TIMER` and `DWT`. Ending with a *Serial Wire Output* (SWO) to display boot info to the console.

## Hello world

Well done soldier! You've survived the tedious Rust setup chapters. Now is the time to write the application everybody writes as their first program - Hello world. Though it won't actually write any message to the screen as it does not have it and the serial communication interface is not initialized. Remember there is no *BIOS* facilitating a text screen at memory address `0xB8000` . For now we'll have to use a single IO pin configured as output to toggle the LED.

## Toggling the LED

When we want to toggle the LED we need to find out from the schematics the pin number as well as the port it is attached to.

![LEDS](/home/cylon2p0/Guides/cortex_guide/cortex_book/img/leds.png)

> The LED is turned on when we apply set pin output high and off when the output is set low.

In our case it is PD12 (Pin 12 on Port D). Therefore we need to configure the GPIOD peripheral in software. But before we can access these registers we need to enable them as they have gated clocks.

The *GPIOD* peripheral is attached to *AHB1* where the frequency can reach 168 MHz as the figure bellow shows.

![AHB1](/home/cylon2p0/Guides/cortex_guide/cortex_book/img/ahb1.png)

To enable the clocks we need to have a look at *RCC* (used previously to find out the cause of reset). The *ARM* specification states the peripheral should first be reset before being enabled.

The registers of the peripheral

**TODO - Check the PORT and NM, add figure of the schematics**

> Pins PA15, PA14, PA13 and PB4 are reserved for JTAG, and are used debugging.

## Clock system

**TODO: Have a look at clock sources and sinks**

*STM32F4* family of microcontrollers has several clock sources.

- *Low Speed Internal* (LSI)
- *Low Speed External* (LSE)
- *High Speed Internal* (HSI)
- *High Speed External* (HSE)

Beside these 4 clock sources we also have *PLL*.

## RCC Peripheral

When reading the section of reference manual regarding the *GPIO* peripheral we came across reference to the *AHB* clock, which needs to be enabled for the I/O port to work at all.

I don't want to describe the *RCC* in detail right here, so we just search for a configuration bit `IOPDEN` (*IO-PORT-D-Enable*) which enables port D clock.

**TODO: Run main as closure with different speed that will be settled in reset_handler**

In the Reference manual we can find description of the *GPIO* registers:

- *RCC AHB1 peripheral reset register* (RCC_AHB1RSTR)
  - GPIODRST: IO port D reset (Bit 3)
- *RCC AHB1 peripheral clock register* (RCC_AHB1ENR)
  - GPIODEN: IO port D clock enable (Bit 3)

## Turn on the LED

Armed with all that knowledge let's turn on our blue LED.

```rust
// chapter5/src/main.rs

#[no_mangle]
pub fn main () -> ! {
    
	// Aquire the RCC peripheral
	let rcc = RCC::init();

    // Reset port GPIOD on AHB1 bus
    rcc.ahb1rst.modify(hal::rcc::AHB1RST::GPIODRST::SET);
    // Release reset port GPIOD on AHB1 bus
    rcc.ahb1rst.modify(hal::rcc::AHB1RST::GPIODRST::CLEAR);
    // Gate clock for GPIOD on AHB1 bus
    rcc.ahb1en.modify(hal::rcc::AHB1EN::GPIODEN::SET);

    // Aquire the GPIOD peripheral
    let gpiod = GPIO::init(hal::gpio::Port::PortD);

    // Set mode (output) for pin 15
    gpiod.mode.modify(hal::gpio::MODE::MODER15::OUTPUT);
    // Set max. frequency (100 MHz) for pin 15
    gpiod.ospeed.modify(hal::gpio::OSPEED::OSPEEDR15::HIGH);
    // Turn on blue LED on pin 15
    gpiod.od.modify(hal::gpio::OD::ODR15::SET);
    
    loop{}
	}
```

Of course now we fact the task to toggle the LED. We could implement a loop with `NOP` (No operation instruction) to delay the microcontroller but there is even a better way.

## Keeping track of time

**TODO**: Timer register definition in documentation.

**TODO: STM32F4 discovery runs off 16MHz RC internal oscillator**

```
tim_update_frequency = TIM_CLK/(TIM_PSC+1)/(TIM_ARR + 1) 
```

**TODO: Const evaluation of expression to calculate the psc and arr**



```rust
#[no_mangle]
pub fn main () -> {
    ...
    rcc.apb1rst.modify(hal::rcc::APB1RST::TIM6RST::SET);
    // Release reset port TIM6 on APB1 bus
    rcc.apb1rst.modify(hal::rcc::APB1RST::TIM6RST::CLEAR);
    // Gate clock for TIM6 on APB1 bus
    rcc.apb1en.modify(hal::rcc::APB1EN::TIM6EN::SET);

    let tim6 = TIM::init();

    // Allow preload of auto-reload
    tim6.cr1.modify(hal::tim::CR1::APRE::SET);
    // Auto-reload value
    tim6.arr.set(65534);
    // Prescaler
    tim6.psc.set(120);
    // Enable timer
    tim6.cr1.modify(hal::tim::CR1::CEN::SET);
    // Generate update event
    tim6.egr.modify(hal::tim::EGR::UG::SET);
    ...
    }
```

## Trespassing

So we designed an API that allows us (finally) to toggle a LED at certain frequency. We do this by dereferencing a block of 32-bit registers. Now imagine that someone else will write a code that needs to access to the same port. So he instantiates the `GPIOD` peripheral and is able to modify any of the registers. Scary right? The root cause here is in fact that `GPIOD` is mutable global variable- leading to *data races*.

> There is a ‘data race’ when two or more pointers access the same memory location at the same time, where at least one of them is writing, and the operations are not synchronized.

Therefore, let's define rules to enforce in order to get rid of data races:

- Every register access must use `volatile` methods, either read or write, as the value at the memory address can change at any time
- We can have only either:
  - One exact mutable reference (`&mut T`) to the peripheral
  - One or more references (`&T`) to the peripheral

The first rule is clear. But, does the second rule sound familiar? I guess it should as it's exactly what the *borrow checker* in Rust does. In fact the same rule is mentioned in the [book].

The only requirement we must fulfill is to have exactly one instance of the peripheral - this is granted by the hardware. If only the peripheral wasn't a mutable global variable...

> Borrow checker is unable to track references and ownership of global variables.

To ensure the requirement also in software we'll have to look at software patterns, specifically to [*Singleton Pattern*].  To implementation of the pattern will rely on `Option<T>`. 

- The first call to `take()` will return `Some<T>`
- Additional calls to `take()` will return `None` 

At first it might seem ridiculous to change one global variable with another but although the Singleton pattern still allows global access to **itself** . However, the access to the peripheral is guarded and obeys the defined rules. 

Also with the introduction of Singleton the `unsafe` block from each peripheral instantiation is moved into the `take` method making our code even safer. 

Now through the call to `take` and the help of the *borrow checker* we can pass around safetly ownership of the peripherals or offer immutable or mutable references. 

> There is only a small run-time overhead while we need to make call to `take()` - small price to pay as the rest of the code is checked by the *borrow checker* during compilation. As a result we got rid of the issue with *data races*.

**TODO: Finish singleton**

**TODO: What about e.g. timer channel and interrupt - we want to set the value of timer channel but we need to clear the bit in timer during interrupt -> borrow just the neccessary part not whole**

## Pins on the market

**TODO: Encapsulation of GPIO API**

Free of the *data races* let's look at the API of the GPIO and its ergonomics. We get reference to a block of 32-bit registers of the `GPIOD` peripheral. Despite being very convenient to work with, it also comes with a flaw as each register contains fields that represent all of the other 15 out of 16 pins connected to the Port (some configuration might be split into 2 registers, but it's still concern).

To improve the API let's rethink the API. Suppose that now we also modify the definiton of the `Port` to hold also the pins in an array.

**TODO: Implementation of the method to return the Pin**

```rust
#[repr(C)]
struct Port{
    gpio: GPIO,
    pins: Pin,
} 
```

Then we define also the `Pin` as:

```rust
enum Pin {
    P0
    P1,
    P2,
    P3,
    P4,
    P5,
    P6,
    P7,
    P8,
    P9,
    P10,
    P11,
    P12,
    P13,
    P14,
    P15
}
```

By enumerating the Pins and putting them into array we are now able to borrow them, therefore eliminate bugs made by the programmers. where the pin is later reconfigured - this is now guarded by the compiler.

**TODO: Return the pin representation**

```rust
struct Pin<'a> {
  idx: u8,
  gpio: &'a GPIO
}
```

Before we define a metods for the `Pin` we should thing about another type of errors the programmer can make.

If we define all the metods on the pin it will mean that the programmer is able to enable pull-up when the pin is configured as analog to convert the value on the pin into the digital representation.

The reason for this is that we defined the method for the Pin in the `impl` block.

In C the responsibility rests on the shoulders of programmer. Could we leverage the Rust type system yet again? Indeed we can. The answer is the *Generic data types*.

```rust
let mut gpiod = p.GPIOD.split(&mut rcc.ahb);

let mut pd12: PD12<Output<PushPull>> = gpioe.pd12.into_push_pull_output(&mut gpioe.moder, &mut gpioe.otyper);
let mut pd13: PD13<Output<PushPull>> = gpioe.pd13.into_push_pull_output(&mut gpioe.moder, &mut gpioe.otyper);

pd12.set_high();
pd13.set_high();
```

If we try e.g. the code below we get compile error:

```rust
let mut pd12: PD12<Input<Floating>> = gpiod.pd12;
pd12.set_high();
```

The refactored type system allows us also to get rid of another common bug. In code without borrow checker you can reference pin and configure it one way. Later you reference it again somewhere else and configure it another way. Physically this is not possible as the pin cannot be in two states - Ownership and move semantics guarantees that there is a single representation in code.

```rust
let mut pd12: PD12<Output<PushPull>> = gpioe.pd12.into_push_pull_output(&mut gpioe.moder, &mut gpioe.otyper);
let mut pd12_later = pd12.into_floating_input(&mut gpioe.moder, &mut gpioe.pupdr);
pd12.set_high();
```

**TODO: Same principle with TIMER, DMA channels**

Though this approach carries also a downside. Each `Pin` will now occupy a piece of our precious `RAM`. Why `RAM` and `FLASH`? We used *Generic data types* and we convert between the `Pin` type - so we modify the variable. 

> The solution to this might be to write macro and implement each `Pin`  as an empty struct. The Rust compiler will be happy as it has variable to track and the macros would implement the methods by directly writing to GPIO registers, thus removing the need to keep the address of the GPIO peripheral and index of the pin.

## Alternate configuration

We should keep in mind that each *GPIO* can be configured either as:

- *Input*
- *Output*
- *Analog*
- *Alternate function*

The last mode is pretty interesting as we'll need it in the next chapters.

**TODO: Figure of GPIO pin**

With the advancement in silicon manufacturing the vendors are able to put more transistors on chip - Moor's Law. As a result the chip is able to offer more functionality - Ethernet, USB, memory and graphics drivers ... However, the amount of pins remain the same. Most of the time we don't need every functionality the chip offers to the outside world. Therefore each pin contains a multiplexor circuit that select which functionality will be used.

The list of alternate functions for each GPIO pin is stored [here]. We'll now implement it in our *GPIO HAL API*.

**TODO: Alternate function signals table file**

Yet again we want to leverage the Rust type system and ensure during the compilation that the alternate signal is bound to the correct pin. 

**TODO: Excerpt of bitfield defintion**

```rust
register_bitfields! [u32,
                     GPIOA_AFL [
                         PA0 OFFSET(0) NUMBITS(4) [
                             TIM2_CH1_ETR = 1,
                             TIM5_CH1 = 2,
                             TIM5_ETR = 3,
                             USART2_CTS = 7,
                             UART4_TX = 8,
                             ETH_MII_CRS = 11],
                         ...
```

### Device specific files

**TODO: Move device specific parts into separate file**