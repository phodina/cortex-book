 # Bed time story

## Power modes

Define compromise between low-power consumption, short startup time and available wake-up sources.

In the application note[AN4364]

By default, and after power-on or a system reset, the STM32F4 is in Run mode, which is a
fully active mode that consumes much power even when performing minor tasks. Lowpower
modes are available to save power when the CPU does not need to be kept running.

### Sleep mode:

– Only the CPU clock is stopped.
The Cortex-M4 clock is stopped and the peripherals are kept running. The current
consumption increases with the clock frequency. As in Run mode, the user should be
aware of system configuration rules that concern the system clock and voltage
regulator scales.

### Stop mode:

– Lowest power consumption while all the SRAM and registers are kept.
– PLL, HSI, HSE are disabled.
– All clocks in 1.2 V domain are switched-off.
– Voltage regulator is working in Normal mode or Low-power mode.
– Flash memory is working in Stop mode or Deep-power down mode.
The Cortex-M4 core is stopped and the clocks are switched off (the PLL, the HSI and
the HSE are disabled). SRAM and register content are kept. All I/O pins keep the same
state as the Run mode. The voltage regulator is working in Normal or Low-power mode
and the Flash memory can be configured in Stop mode or Deep-power down mode to
save more static power.

### Standby mode:

– The lowest power consumption.
– The 1.2 domain is powered off (regulator is disabled).
– SRAM and register contents are lost except in the backup domain.
The Cortex-M4 core is stopped and the clocks are switched off. The voltage regulator is
disabled and the 1.2 V domain is powered-off. SRAM and register contents are lost
except for registers in the Backup domain (RTC registers, RTC backup register and
backup SRAM), and Standby circuitry. This mode has the lowest current consumption,
which depends on the configuration of both backup SRAM (not available for
STM32F401x) and RTC.

### VBAT mode:

– The main digital supply (VDD) is turned off.
– The circuit is supplied through VBAT pin which should be connected to an external
supply voltage (a battery or any other source).
This mode is used only when the main digital supply (VDD) is turned off and the VBAT
pin is connected to an external supply voltage (a battery or any other source). The VBAT
pin powers the Backup domain (RTC registers, RTC backup register and backup
SRAM).

### Voltage regulator power tricks

There are new tricks implemented in STM32F4 MCUs to reduce current consumption by
monitoring the regulator voltage output.

- Over-drive mode: this mode allows the CPU and the core logic to operate at a higher

frequency than the normal mode for a given voltage scaling (scale1, scale2 or scale3).
This mode is available only with STM32F429x/439x devices.

- Low-voltage mode: in Stop mode, we can further reduce the voltage output of the lowpower

regulator by using the Low-voltage mode. This mode is only available in Stop
mode with STM32F429x/439x and STM32F401x devices.

- Under-drive mode: the 1.2 V domain is preserved in Reduced-leakage mode. This

mode is only available in Stop mode when the main regulator or the low-power
regulator is in Low-voltage mode. Under-drive mode is available only with
STM32F429x/439x devices

### Jumper for Idd consumption measuring

https://www.embedded.com/print/4230085

https://www.embedded.com/print/4442697

## Time to sleep ...

TODO: we'll write some stuff in assembly.

## ... time to wake up

# Summary

Now we understand the architecture of ARM Cortex-M4 core, can access special registers and write code in assembly. The [following](chapter_4.html) post will cover the booting, interrupt handlers and faults.	