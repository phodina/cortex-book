# The Grand design

## The device-specific details

Before we dive into the nitty-gritty details I'll list some documentation which will cover the *STM32* families as I'm most familiar with the them. Other vendors have similar documentation.

**TODO: Mention ARM programming manual**

**TODO: Mention ST Reference manual**

**TODO: Mention ST datasheet (alternate functions)**

**TODO: Mention ST board manual**



## GPIO registers

In the next post we'll finally toggle LED. In order to do that we need to define the registers of the *General Purpose Input Output* (GPIO) peripheral.

**TODO:  Format of the registers**

In the Reference manual we can find description of the *GPIO* registers:

- *Mode Register* (MODER) -Each 2 neighbouring bits configures mode of the pin:
  - Input (`b00`)
  - Output (`b01`)
  - Alternate function (`b10`)
  - Analog (`b11`)

- *Output Type Register* (OTYPER) -Only the low 16 bits are used. Each bit belongs to one pin and controls whether pin will be:
  - Push-pull (0)
  - Open-drain (1)
- *Output Speed Register* (OSPEEDR) - Each 2 neighbouring bits configures maximum frequency of the output signal on the pin:
  - Low speed 2MHz (b00)
  - Medium speed 25MHz (b01)
  - Fast speed 50MHz (b10)
  - High speed 100MHz (b11)
- *Pull-Up Pull-Down Register* (PUPDR) - Each 2 neighbouring bits controls whether the pull-up/down resistor is attached:
  - No pull-up/down (b00)
  - Pull-up (b01)
  - Pull-down (b10)
  - Reserved (b11)
- *Input Data Register* (IDR) - Only the low 16 bits are used. Each bit contains current logical level on the input of the pin. Pin should be configured in input mode though the circuitry allows to read it in any mode.

- *Output Data Register* (ODR) - Again, only the low 16 bits are used. Each bit controls the logical level of the corresponding pin. Pin must be configured in output mode.

- *Bit Set Reset Register* (BSRR)

> The register is divided into high and low 16 bits. Low 16 bits *BSR* allow to set the logical level on the corresponding pin. High 16 bits *BRR* clear the logical level.
> **TODO: What happens when we set both clear/set?**

- *Lock Register* (LCKR)

> The low 16 bits allow to lock-down the configuration of the corresponding pin. Bit 17 activates the lock protection which remains till reset.

- *Alternate Function Low & High* (AFRL & AFRH)

> When the pin is configured in alternate function mode, the alternate function can be selected through these two registers. Each pin has 4 bits which select the alternate function.

> Each I/O port bit is freely programmable, however the I/O port registers have to be accessed as 32-bit words

### Peripheral abstraction

To create zero-cost and safe abstraction around GPIO registers and others we'll need add new external crate `register` and import two modules:

- `regs.rs`

  > Definition of operations on the registers

- `macros.rs`

  > Definitions of bitfields

I reused the register definitions from *Tock OS* project which found ingenious way of using Rust type system to define registers. Unfortunately both files are quite long so I won't list them here I'll only show the definition of *GPIO* registers from file `gpio.rs`.

```rust
// gpio.rs

// General Purpose I/O
#[repr(C)]
pub struct GPIO {
	// Mode
	pub mode: ReadWrite<u32, MODE::Register>,
	// Output type
	otype: ReadWrite<u32, OTYPE::Register>,
	// Output speed
	ospeed: ReadWrite<u32, OSPEED::Register>,
	// Pull-up/pull-down
	pupd: ReadWrite<u32, PUPD::Register>,
	// Input data
	id: ReadWrite<u32, ID::Register>,
	// Output data
	od: ReadWrite<u32, OD::Register>,
	// Bit set/reset
	bsr: ReadWrite<u32, BSR::Register>,
	// Configuration lock register
	lck: ReadWrite<u32, LCK::Register>,
	// Alternate function low
	afl: ReadWrite<u32, AFL::Register>,
	// Alternate function high 
	afh: ReadWrite<u32, AFH::Register>
}
```

Firstly you should notice `#[repr(C)]` which tells *Rust* to use the same layout in memory as *C*. Next each member is declared either `ReadWrite`, `Read-Only` or `Write-Only` which mirrors the register access and gives compilation error when you try to do invalid read or write access. The last feature is the bitfield association which allows the developer to use only the bits defined on the particular register.

Here you can see excerpt of the bitfield for `GPIO`:

```rust
// hal/src/gpio.rs

register_bitfields! [u32,
    MODE [
        MODER0 OFFSET(0) NUMBITS(2) [
        	INPUT = 0b00,
        	OUTPUT = 0b01,
        	ALTERNATE = 0b10,
        	ANALOG = 0b11
        	],
      	MODER1 OFFSET(0) NUMBITS(2) [],
		...
    ],
    OTYPE [
        OT0 OFFSET(0) NUMBITS(1) [
        	PUSH_PULL = 0b0,
        	OPEN_DRAIN = 0b1
        	],
        OT1 OFFSET(1) NUMBITS(1) [],
      	...
    ],
```

The bitfields are 32-bits in size and are divided into namespaces e.g. `MODE`. There you'll find the definition for the individual bits - their name, offset and span.

The interface design of the `register` crate allows the compiler to catch common bugs through type checking. For each register we defined descriptive group like `MODE` therefore these bits will work only with a register of the type `ReadWrite<_, MODE>`. 

If we would try to use `OTYPE` bitfields for `mode` register the code will not compile:

```rust
gpio.mode.modify(OSPEED::OT0::PUSH_PULL);
```

Don't forget to add the dependency in `Cargo.toml`:

```toml
# Cargo.toml
...
[dependencies]
r0 = "0.2.2"
register = "0.2.0"
```

And the `main.rs:`

```rust
// main.rs
...

#[macro_use]
extern crate register;
```

The attribute `macro_use` exports the macros form that crate and allows us to use them.

> The defined interface is how interactions with the hardware are made, no matter what language is used, whether that language is Assembly, C, or Rust. Therefore it can be compiled into a library and linked into different applications.

### Naming conventions

The enclosed `Readme.md` document also recommends naming conventions for register definition:

- The name of the register should be be lowercase abbreviation as written in the datasheet. (e.g. *Control Register* -> `cr`)
- On the other hand the name of the bitfield should be written as long decriptive camelcase name without the word *register*. (e.g. *Control Register* -> `Control`)
- The name of the field should be capitalized abbreviated field name. (e.g. *Enable Transmitter* -> `ENTX`)
- Each field value should be a descriptive camelcase name   (e.g. *EnableTransmitter* ->`EnableTx = 0`)

### Register methods

With the registers and their bit defined it's time to investigate what methods can we call on them. Suppose we have immutable instance of our GPIO peripheral - `gpio`.

- We can perform a raw access

  ```rust
  // Get or set the raw value of the register directly
  // `mode` will contain a raw u32 value of the MODE register
  // Type annotation is not necessary, but it's provided for clarity here and afterwards
  let mode: u32 = gpio.mode.get();
  
  // Set the raw value of the register directly
  gpio.mode.set(0x0000ffff);
  ```

- We can read the register

  ```rust
  // `ot1` will contain the value of the OT1 field, PUSH_PULL or OPEN_DRAIN.
  let ot1: u8 = gpio.ot.read(OT::OT1);
  
  // `id5` will be 0 or 1
  let id5: u8 = gpio.id.read(ID::ID5);
  ```

- We can write the register

  ```rust
  // All unspecified fields are overwritten to zero.
  gpio.or.write(OD::OD10.val(1)); // implictly sets all other bits to zero
  ```

- We can modify the register

  ```rust
  // Write a value to a bitfield without altering the values in other fields:
  gpio.mode.modify(MODE::MODER2.val(1)); // All other MODER x ( where x is 0 - 15, except 2) will not be touched, but MODER2 will be set OUTPUT
  
  // Named constants can be used instead of the raw values:
  gpio.mode.modify(MODE::MODER2::OUTPUT);
  
  // Another example of writing a field with a raw value:
  gpio.mode.modify(MODE::MODER2.val(0)); // Leaves other MODER x unchanged
  
  // Any number of non-overlapping fields can be combined:
  // Write multiple values at once, without altering other fields:
  gpio.mode.modify(MODE::MODER2::OUTPUT + MODE::MODER13::INPUT);
  ```

- Single bit

  ```rust
  // For one-bit fields, the named values SET and CLEAR are automatically
  // defined:
  gpio.od.modify(OD::OD5::SET);
  gpio.id.modify(OD::OD5::CLEAR);
  
  // All unspecified fields are overwritten to zero.
  gpio.or.write(OD::OD5::SET);
  ```

- Checking single bit

  ```rust
  // For one-bit fields, easily check if they are set or clear:
  let lck10: bool = gpio.lck.is_set(LCK::LCK10);
  ```

- Checking multiple bits

  ```rust
  // You can also query a specific register state easily with `matches_[any|all]`:
  
  // Doesn't care about the state of any field except ID10 and ID15 i.e. the two buttons must be pressed at the same time
  let buttons: bool = gpio.id.matches_all(ID::ID10 + ID::ID15);
  
  // This is very useful for awaiting for a specific condition:
  while !gpio.id.matches_all(ID::ID10::SET + ID::ID15::CLEAR) {}
  
  // Or for checking whether any pin is configured as output:
  let outputs = gpio.mode.matches_any(MODE::MODE5::OUTPUT + MODE::MODE12::OUTPUT);
  ```

- Make local copy

  ```rust
  // Create a copy of the register value as a local variable.
  let local_lck = gpio.lck.extract();
  
  // All the functions for a ReadOnly register work as expected.
  let lck10: bool = local_lck.is_set(LCK::LCK10);
  ```

Keep in mind that:

Note that `modify` performs exactly one volatile load and one volatile store,
`write` performs exactly one volatile store, and `read` performs exactly one
volatile load. Thus, you are ensured that a single call will set or query all
fields simultaneously.

- `read` - performs exactly one volatile load
- `write` - performs exactly one volatile store
- `modify` - performs exactly one volatile load and one volatile store

### Implementing static dispatch

The function then uses the object reference in the same manner as the implicit this parameter in conventional object-oriented languages such as *Java* or *C++*.

```rust
impl GPIO {

  pub fn set_mode(&self, pin: Pin) {
    ...
  }

  pub fn set_ospeed(&self, pin: Pin) {
    ...
  }
}

```

### Instantiating a MMIO object

Now that we have shown that *MMIO*s can be represented as objects defined by
structs, we have to consider how to instantiate them. In the *OOP* object is usually created with a *constructor* and deallocated with a *destructor*.

> The constructor is responsible for allocating the object and initializing the fields with values.

> The destructor is responsible for deallocating the object and any other member objects that it owns.

*MMIO* devices have a fixed position in the memory and do not need to be allocated, and they also generally do not have any owned members. Therefore the constructor-destructor pattern is not applicable
for *MMIO*s, but we still need to instantiate the variable that holds the reference to the *MMIO*.

> The destructor could in theory de-initialize the MMIO though this is not that common in embedded world.

> Rust doesn't guarantee the destructor to run. 

There are currently two unsafe ways how to instantiate a *MMIO* peripheral:

- rust reference to raw pointer dereference (`&*(addr as *const MMIO_Registers)`)
- `mem::transmute`

There are two types of raw pointers:

- A **mut T* is a raw pointer to a *T* that permits modifying its referent
- A **const T* is a raw pointer to a *T* that permits reading its referent

> Unlike boxes and references, raw pointers can be null, like NULL in C or nullptr in C++. This can be represented by `core::ptr::null`

> A raw pointer to an unsized type is a fat pointer. A *const [u8] pointer includes length along with an address.

Some peculiarities about raw pointers:

- The `.` operator will not implicitly dereference raw pointer; you have to dereference it yourself as (\*ptr).field or (\*ptr).method
- Raw pointers do not implement `Deref`
- Operators such as `==` and `>` compare the addresses; not the value of the referents
- Formatting traits such `std::fmt::Debug` and `std::fmt::Pointer` show raw pointers as hexadecimal addresses without dereferencing them
- Pointer arithmetics can be done via `offset` and `wrapping_offset` methods
- We can check for null pointer by `is_null` or `as_ref`, which returns `Option<&'a T>`
- Raw pointers can be obtained through `as_ptr` and `as_mut_ptr` methods
- Owning pointer types like Box, Rc and Arc have `into_raw` and `from_raw` methods that convert to and from raw pointers
- Unlike references raw pointers are not Send nor Sync as any type that includes them does not implement these traits by default.  

Let there be the dragons:

- Dereferencing raw pointers or dangling pointers is undefined behavior
- Dereferencing raw pointers that are not aligned is undefined behavior
- You must not violate any invariant of the referent type

Call to `core::mem::sizeof::<T>()` will return the size of a value of type T and `core::mem:align_of::<T>()` its required alignment for *Sized* type.

Call to `core::mem::size_of_val::<T>()` will return the size of a value of type T and `core::mem:align_of_val::<T>()` its required alignment for either *Sized* or *Unsized* types.

```rust
core::ptr::read(src)
core::ptr::write(dest,value)
core::ptr::copy(src,dst,count)
core::ptr::copy_nonoverlapping(src,dst,count)
core::ptr::read_unaligned()
core::ptr::write_unaligned()
core::ptr::read_volatile()
core::ptr::write_volatile()
core::ptr::drop_in_place() // works like drop(core::ptr::read(ptr)) but works on unsized types
```



**TODO: Check RFC**

There is a [RFC 911] pushing `mem::transmute` to  become `const fn` in the future - hopefully then creating proper reference to registers directly. 

Until then let's look at the creation of the `GPIO` peripheral by using a raw pointer:

**TODO: Remove unsafe** https://github.com/tock/tock/issues/385

```rust
// hal/src/gpio.rs

const GPIO_BASE: *mut GPIO = 0x40020000 as *mut GPIO;

impl GPIO {
  fn init () -> GPIO {
    unsafe {GPIO_BASE.as_mut().unwrap()}
  }
}
```

#### Static reference

To make it little safer we'll use `StaticRef` from the *Tock* project providing a kind of safe wrapper.

```rust
// hal/src/gpio.rs
impl GPIO {
  pub fn init(port: Port) -> super::staticref::StaticRef<GPIO> {
    unsafe {super::staticref::StaticRef::new((port as usize) as *const GPIO) }
  }
}
```

As a result we can cast the memory address to a reference, something that is not allowed with a raw pointer - even when a pointer is a compile-time constant.

Let's examine the definition of `StaticRef`:

```rust
// cortex/src/staticref.rs

//! Wrapper type for safe pointers to static memory.

use core::ops::Deref;

/// A pointer to statically allocated mutable data such as memory mapped I/O
/// registers.
///
/// This is a simple wrapper around a raw pointer that encapsulates an unsafe
/// dereference in a safe manner. It serve the role of creating a `&'static T`
/// given a raw address and acts similarly to `extern` definitions, except
/// `StaticRef` is subject to module and crate boundaries, while `extern`
/// definitions can be imported anywhere.
#[derive(Debug)]
pub struct StaticRef<T> {
    ptr: *const T,
}

impl<T> StaticRef<T> {
    /// Create a new `StaticRef` from a raw pointer
    ///
    /// ## Safety
    ///
    /// Callers must pass in a reference to statically allocated memory which
    /// does not overlap with other values.
    pub const unsafe fn new(ptr: *const T) -> StaticRef<T> {
        StaticRef { ptr: ptr }
    }
}

impl<T> Clone for StaticRef<T> {
    fn clone(&self) -> Self {
        StaticRef { ptr: self.ptr }
    }
}

impl<T> Copy for StaticRef<T> {}

impl<T> Deref for StaticRef<T> {
    type Target = T;
    fn deref(&self) -> &'static T {
        unsafe { &*self.ptr }
    }
}
```

Beside wrapping the pointer we also implement several traits:

**TODO: Add links to traits**

- `Copy` trait - We need to implement it at least as an empty trait for `Clone` .
- `Clone` trait - We just create a new reference. 
- `Deref` trait - We want immutable dereferencing operations.

> On multicore system we would also implement `Send` and `Sync` trait to move the object atomically from one core to another

### Multiple Ports

Great now we are able to bend the pin to our will. However, the *STM32F407VG* chip comes in *LQFP100* package where 82 out of the 100 pins are *GPIO*s. In the code we access only 16 pins!

The GPIOs are group into Ports. As there are several Ports available each has to be mapped to a different base address. Let's encode this information into `enum`:

```rust
pub enum Port {
    PortA = 0x40020000,
    PortB = 0x40020400,
    PortC = 0x40020800,
    PortD = 0x40020C00,
    PortE = 0x40021000,
    PortF = 0x40021400,
    PortG = 0x40021800,
    PortH = 0x40021C00,
    PortI = 0x40022000,
    PortJ = 0x40022400,
    PortK = 0x40022800,
}
```

Now we tweak the `init` function for struct `GPIO`:

```rust
impl GPIO {
  pub fn init(port: Port) -> super::staticref::StaticRef<GPIO> {
        unsafe {super::staticref::StaticRef::new((port as usize) as *const GPIO) }
    }
}
```

