# Lift off

## Linker

The process of building crate consists more or less of two steps - compiling and linking.

During compilation we process source code written in *Rust* and turn it into an intermediate file - object file. In *Rust* the minimal compilation unit is the crate. It can have only one `.rs` file or multiple but they end up in one object file. *Rust* also has a special format for dependencies `.rlib` which is and object file with some meta-data attached. To get the final binary we need to merge the object files together somehow using the linker.

*Cortex-M4* family has predefined memory layout which we'll address by custom linker script. The content of the script will tell linker where to place different sections of the executable.

- symbol

  > either a function or a static variable(has a name, a start address and occupies some space memory)

- section

  > a collection of symbols stored in contiguous memory

- region

  > a span of memory that's described using a start address and a length (in bytes)

- object files

  > one or more inputs for the linker.

The resulting executable file will be *Executable and Linkable Format* (ELF) which also happens to be the format *Linux* executables use.

> Dynamic linking is not that common in embedded world mainly due to the size of the storage, original firmware sicks forever ... and the benefits it provides are negligible 

### Linker script

```
ENTRY(reset_handler)

MEMORY
{
    FLASH (rx) : ORIGIN = 0x00000000, LENGTH = 256K
    RAM  (rwx) : ORIGIN = 0x20000000, LENGTH = 64K
}

PROVIDE(_stack_start = ORIGIN(RAM) + LENGTH(RAM));

SECTIONS
{
    .text : {
        __STACK_START = .;
    	   LONG(_stack_start);

    	   KEEP(*(.vector_table.reset_vector));
	   __reset_vector = ABSOLUTE(.);

    	   KEEP(*(.vector_table.exceptions));
   	    __eexceptions = ABSOLUTE(.);

	   KEEP(*(.vector_table.interrupts));
	   __einterrupts = ABSOLUTE(.);

       *(.text*)
    } > FLASH = 0xFF

    .bss : ALIGN(4)
    {
        _sbss = .;
        *(.bss.*);
       _ebss = ALIGN(4);
    } > RAM

    .data : ALIGN(4)
    {
    _sdata = .;
    *(.data.*);
    _edata = ALIGN(4);
     } > RAM AT > FLASH

    .ARM.exidx :
     {
     *(.ARM.exidx*)
     } > RAM

    _sidata = LOADADDR(.data);
}
```

Ok, let's go through the linker script together.

The `ENTRY(reset_handler)` indicates the programs entry point. It's important to mark it as the linker remove any symbol that is not referenced by the entry point as the *dead code*.

- The entry point must be marked `pub` so that we get *global symbol*
- We need to force the compiler to use *C ABI* therefore we need to use *extern "C"*
- As this is the entry point in an embedded application we must never return from this function therefore the func has to have signature *fn func() -> !*

Then we need to define the available areas inside `MEMORY`. `FLASH` memory is defined as `readable (r)` and `executable (x)`, but `writable (w)` as we don't generally want a self modifying code.

> Even if you define the FLASH memory as writable you'll be unable to modify it on STM32 MCUs as the FLASH is by default protected and you need to unlock it first. As an additional protection some pages of FLASH might be also protected - bits are set in special memory area called OPTION BYTES.

> We use `FLASH = 0xFF` to reduce the wear leveling of the memory and faster programming time - the default erased value of FLASH will 0xFF.

> Throughout the script we utilize a special symbol `.` which holds the current address.

On the other hand we define `RAM` memory as `readable (r)` and `writable (w)`, but not `executable (x)` - protection against malicious code and programming mistakes (though Rust is safe, we'll use a lot of unsafe calls).

> We could make exception and divide the RAM into 2 parts- one for data and the other for code. The data would have `read` and `write` attributes. The code would be `read` and `execute`. This could be e.g. the exceptions handlers or other functions that need to be executed fast. The FLASH memory is able to keep up with CPU up to certain point - then we'll have to add wait state as the memory access takes more CPU cycles and increases with frequency.

`_stack_start = ORIGIN(RAM) + LENGTH(RAM)` calculates the address of stack pointer as the end of RAM (stack grows down) and saves it into `_stack_start` symbol. Macro `PROVIDE` allows us to have a default address that can be overwritten by defining `_stack_start` symbol in code.

We place several sections in `.text` (Vector table, exceptions and interrupts will be discussed in next chapter):

- `_stack_start`

> The first item in Vector table is the address of stack pointer

- `.vector_table.reset_vector`

> The second item in Vector table is the reset handler

- `.vector_table.exceptions`

> The next 14 items in Vector table are exception handlers defined by ARM

- `.vector_table.interrupts`

> The next up to 240 items in Vector table are interrupt handlers defined by the vendors

- `.text*`

> Finally we place all the executable code

We can inspect the object files using `nm` (you'll have probably different name like `cortex-<HASH>`)   .

```shell
nm --demangle target/thumbv7em-none-eabi/debug/deps/cortex-443a9256d88171a6
20000000 A _ebss
20000000 A _edata
00000040 A __eexceptions
00000400 A __einterrupts
00000008 T __EXCEPTIONS
00000040 T INTERRUPTS
00000409 T main
00000401 T reset_handler
00000008 A __reset_vector
00000004 T __RESET_VECTOR
20000000 B _sbss
20000000 D _sdata
0000040c A _sidata
20010000 A _stack_start
00000000 T __STACK_START
00000400 t $t.0
00000408 t $t.1
0000040a t $t.3
0000040b t cortex::default_handler
```

In the listing we can see in the first column the address, in the second tag and the third contains name of the symbol.
Let's look at the tags:

- A - The symbol’s value is absolute, and will not be changed by further linking

  > There are multiple examples such as `_stack_start`, `reset_handler`, etc. Most of them are part of Vector table.

- B - The symbol is in the uninitialized data section

  > In our case we can see the `_sbss` section at address `0x20000000`

- D - The symbol is in the initialized data section

  > We have the `_sdata` section at address `20000000`

- T - The symbol is in the text (code) section

  > There are multiple symbols e.g. `reset_handler`, `main`, etc. 

To see information about the *ELF* file we can use `readelf` (or `objdump`)

```shell
readelf -A <executable.elf>
```

### Cargo-binutils

As an alternative to the regular `GCC binutils` tools we could use crate `cargo-binutils`.

```shell
$ cargo install cargo-binutils
```

`cargo-binutils` is a collection of Cargo subcommands that wraps the *LLVM* tools which are shipped with Rust. Among the tools we can find versions of `objdump`, `size` and `nm`. In order to use the tools we need to download them through `rustup`:

```shell
$ rustup component add llvm-tools-preview
```

The benefit is that the tools will support all the architectures supoorted by `rustc` as they share the same *LLVM* backend.

**TODO: Example of Cargo-binutils size**

## Reset handler

In the last chapter you might have notice I referred to `reset_handler` symbol as the entry point of the executable. However, we used `_start` symbol for that. Therefore we'll rename `_start` function to `reset_handler` to adhere to *ARM Cortex-M* specification. In order to run the code on the board we need to set the stack pointer. The stack pointer is stored as first item in vector table and is loaded by the hardware after reset. To keep things simple we define a vector table as an array of function pointers except for the first item:

> We apply attribute `no_mangle` on reset_handler as we use the name of the function in linker script and Rust would otherwise mangle the function name to make it unique. 

In the following chapter I'll explain the *memory layout* and the *startup* sequence. For now you should know that the FLASH memory starts with a *Vector table* (will be explained later). The *Vector table* contains the address of stack pointer and addresses of interrupts/handler. Therefore we need to create a dummy handler `default_handler` and use it for every interrupt/handler source in the Vector table (256 items - first is stack pointer).

```rust
#[link_section = ".vector_table.reset_vector"]
#[no_mangle]
pub static __RESET_VECTOR: unsafe extern "C" fn() -> ! = reset_handler;

#[no_mangle]
pub extern "C" fn reset_handler() -> ! {
    main();
    loop{}
}

#[link_section = ".vector_table.exceptions"]
#[no_mangle]
pub static __EXCEPTIONS: [extern "C" fn(); 14] = [default_handler; 14];

#[link_section = ".vector_table.interrupts"]
#[no_mangle]
pub static INTERRUPTS: [extern "C" fn(); 240] = [default_handler; 240];

extern "C" fn default_handler() {
  loop{}
}
```

> Note that Cortex-M4 does not support ARM excution mode only the Thumb2 mode. Therefore all the addresses need to have set the lowest bit. Well dive into Thumb2 execution mode in next chapter.

## Preparing the board

**TODO: Figure showing OpenOCD, GDB and discovery kit connection**

*GDB* therefore connects to *OpenOCD* and sends it commands through a local network socket (you can use also remote if the board is not on your workbench). *OpenOCD* then talks over the transport interface (USB in our case) to the hardware debugger which carries the commands on the target through the debug interface (*JTAG*/*SWD*)

**TODO:  Check the jumpers + correct usb port**

## Flashing the Discovery kit

To run the never ending loop (blinking LED would make the printer run out of paper) on the discovery kit we'll need a GDB server - *OpenOCD*.  The following figure shows our setup:

```shell
$ openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Error: libusb_open() failed with LIBUSB_ERROR_ACCESS
Error: open failed
in procedure 'init'
in procedure 'ocd_bouncer'
```

As you can see we get an error in *libusb*. You most probably don't have access to the USB device as a user. So you can either run the *OpenOCD* as root, **which is dangerous and I don't recommend it**, or you create a new rule for the *udev* deamon for the ST-Link.

We need to get the ST-Link's *Product ID* (PID) and *Vendor ID* (VID) - these tell the kernel what kind of device is enumerated and which drivers to use. So we run `lsusb`:

```shell
$ lsusb
...
Bus 001 Device 007: ID 0483:3748 STMicroelectronics ST-LINK/V2
...
```

> All ST-Links have VID (0x483) and PID (0x3748). When debugging with multiple ST-Links you can use the serial ID.

No we need to create a new rule in `/etc/udev/rules.d/49-stlink.rules` with following content:

```
ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE="0666"
```

Then just reload the rules:

```shell
# sudo udevadm control --reload-rules
```

Now you can try again. Hopefully the OpenOCD will connect and you'll see output similar to this:

```shell
openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Info : STLINK v2 JTAG v14 API v2 SWIM v0 VID 0x0483 PID 0x3748
Info : using stlink api v2
Info : Target voltage: 2.927771
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints
```

> The configuration scripts we use only attach the debugger to the target without stopping the target.
> You can see that the debugger uses ST-Link version 2 over JTAG and the target supports up to 6 hardware breakpoints and 4 watchpoints - data breakpoints
> Remember that these scripts don't modify the debug core inside the target therefore all the breakpoints and watchpoints will remain after disconnection

### PyOCD

With the rise of the *MBED* framework there has been a new tool there has been a new tool on the (open) market - Python implementation of OpenOCD.

**TODO: Example of PyOCD**

### LLDB

Beside the *GDB* there is also the *LLDB* wich is part of the *LLVM* framework. However, *LLDB* currently does not support bare-metal targets. You would have to add a process stub code similiar to this and recompile the code.

Then you would just select the platform and connect to it:

```shell
(lldb) platform select remote-gdb-server
  Platform: remote-gdb-server
 Connected: no
(lldb) platform connect connect://localhost:4444
  Platform: remote-gdb-server
  Hostname: (null)
 Connected: yes
```

### Flashing the binary

So with the *OpenOCD* running now in one terminal we open another terminal and run the debugger:

```shell
$ arm-none-eabi-gdb target/thumbv7em-none-eabi/debug/cortex
(gdb) target remote :3333
Remote debugging using :3333
(gdb) load
...
(gdb) break main.rs:15
(gdb) continue
Continuing.

Breakpoint 2, reset_handler () at src/main.rs:15
15          loop{}
...
```

Now we loaded the executable to the board and executed it. Of course you won't see anything as there is only a never ending loop for now. That's why we set breakpoint to line 15 in `main.rs`.

## Remarks

**TODO: GDB reset `monitor reset halt`**

> As an alternative you can use tool called STLink which allows you to flash/dump the FLASH memory as well as access option bytes.

> If you can't use any tool on your PC you can use the built-in bootloader over serial connection.

> Newer boards like Nucleo support *USB mass storage class* device type so you can upload the binary by just copying it to the enumerated drive.

> Instead of arm-none-eabi-gdb you can use also gdb-multiarch

### Hardware debugger

You might have notice that on the discovery board there are in fact two STM32s. One is our target - *STM32F407VG* - and then the other one - *STM32F103* . The latter one is on one side connected to the target through *JTAG/SWD* and on the other side to a computer through USB. The ST-Link interprets the commands received over USB into *JTAG/SWD* compliant electrical signals and vice versa.

## No Hardware?

If we don't have a spare Discovery kit or any *STM32* or *ARM Cortex-M* core available we can always fall back to *QEMU* - though *QEMU* does not contain the peripherals just the core. However, there is a project that directly support Cortex-M cores with focus on emulating the boards. One of the supported boards is *STM32F4-Discovery*.

```shell
$ qemu-system-gnuarmeclipse --verbose --verbose --board STM32F4-Discovery \
--mcu STM32F407VG --gdb tcp::1234 -d unimp,guest_errors \
--semihosting-config enable=on,target=native \
--semihosting-cmdline target/thumbv7em-none-eabi/debug/cortex -s -S
```

## Finishing touches

To minimize the executable size, we should always compile with `--release` flag. To optimize the size even further we should use *Link Time Optimization* (LTO). This tells the linker to optimize all the object files in the final executable. The compiler itself can't do this as it does not know the layout and the other object files. We can set this up in `Cargo.toml` release profile:

```toml
# Cargo.toml

[profile.release]
lto = true
```

# Summary

If you managed to read it till this chapter you should have a new binary crate targeted for *ARM Cortex-M4* that will build without any errors. In the [next](chapter_3.html) post.

[ BIOS ]: https://en.wikipedia.org/wiki/BIOS
[The Guide to the Rust Runtime]: https://doc.rust-lang.org/0.12.0/guide-runtime.html
[How rust works without the standard library]: https://www.rust-lang.org/en-US/faq.html#does-rust-work-without-the-standard-library
[Request For Comment]: https://github.com/rust-lang/rfcs/blob/master/text/1184-stabilize-no_std.md
[ Intel ME ]: https://en.wikipedia.org/wiki/Intel_Active_Management_Technology
[ language items ]: https://doc.rust-lang.org/unstable-book/language-features/lang-items.html
[ RFC2070 ]: https://github.com/rust-lang/rfcs/blob/master/text/2070-panic-implementation.md
[ releases ]: https://doc.rust-lang.org/book/second-edition/appendix-07-nightly-rust.html
[ ELF ]: https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
[ Target triples ]: (http://llvm.org/docs/LangRef.html#target-triple)
[ ABI ]: (https://en.wikipedia.org/wiki/Application_binary_interface)
[ LLVM documentation ]: (http://llvm.org/docs/LangRef.html#data-layout)
[ LLD ]: (https://lld.llvm.org/)
[official documentation]: https://doc.rust-lang.org/nightly/nightly-rustc/rustc_target/spec/index.html