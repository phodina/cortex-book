# Launchpad

```
ATTRS{idVendor}=="1cbe", ATTRS{idProduct}=="00fd", MODE="0666"
openocd -f board/ek-tm4c123gxl.cfg

MEMORY
{
   FLASH (rx)  : ORIGIN = 0x00000000, LENGTH = 0x00040000
   RAM   (rwx) : ORIGIN = 0x20000000, LENGTH = 0x00008000
}
```

