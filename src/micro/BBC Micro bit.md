# BBC Micro bit

```
ATTRS{idVendor}=="0d28", ATTRS{idProduct}=="0204", MODE="0666"
openocd -f interface/cmsis-dap.cfg -f target/nrf51.cfg

MEMORY
{
  FLASH (rx) : ORIGIN = 0x00018000, LENGTH = 0x28000
  RAM (rwx) :  ORIGIN = 0x20002000, LENGTH = 0x2000
}
```

