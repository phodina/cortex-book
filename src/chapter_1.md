# Into Rust

**TLDR;**

The goal of this series is to describe all the steps to take when starting in *embedded programming in Rust*. The first several chapters will mainly cover *bare metal programming* - accessing hardware directly without any OS abstractions or services. Nevertheless we'll touch OS stuff in the latter chapters. The selected targets will be *ARM Cortex-M* microcontrollers which are very well supported by the *Rust* programming language.

## Preface
I have a great interest in understanding how low-level things work, how microprocessors work, how to program them, how firmware manages the hardware, how to connect and read values from all kind of sensors, and many more things. Therefore, I have decided to write series of posts about the programming [ARM] microcontrollers in [Rust].

Note that I'm a software engineer by day and in my previous job as well as at the university (though some time ago) I worked on them. This is a chance for me on the one hand a way to pass on the knowledge about *ARM Cortex-M* microprocessors and on the other hand a chance to learn *Rust* in an embedded environment. Beside that I also like both the software as well as the hardware - and here the worlds colide.

Even though I try to explain most of the stuff don't hesitate if you notice anything confusing, or if you have any questions/remarks, drop me an [email] or just create a thread in the forum. I appreciate it as well as the others that might ask the same question but are afraid to ask.

Also as English us not my native language and you'll find some typo or something wrong, feel free to send an [email].

> Note that this isn't official documentation, just learning and sharing knowledge.

## ~~Required~~ knowledge that might help
* Understanding *Rust* code
* Understanding microprocessor

Anyway, if you are just starting to learn such subjects, I will try to explain some parts little more in the following posts. Ok, we're at the end of our simple introduction, and now we can have a look at the microcontrollers and embedded *Rust*.

I've started writing this book at the time of the *Rustc 1.29.0*, and many things might have changed since that time. If there are changes, I will update the posts accordingly. However, I hope there won't be many changes as the *Rust 1.x* promises stability of the *Rust* languages.

## So which microcontroller to choose?
To begin we need to select the appropriate target platform that we'll use for our future development. That's easier said than done as there are numerous platforms with various architectures, level of documentation, support ...

But wait what's a microcontroller? A simple answer would be it's toady a *System on a Chip* ([SoC]) - all the discrete components are inside one chip (or package if you will). Compare that now to your laptop which is built up from several discrete components: a processor, RAM, hard drive (or SSD, they are quite common nowadays), Ethernet port, USB controller, etc.

> SoC approach eases the development of the target system as it will require less parts therefore less time to solder, space on board, points of failures etc.

You can find these chips embedded literally anywhere like coffee machine, washing and vending machines to name a few. Of course also as unit in cars, boats, elevators, etc. facilitating different traits e.g. steering, engine control, breaking, etc. 

One common feature is that these systems operate mostly without user intervention e.g. coffee machine will brew the coffee on its own regulating the amount of beans, temperature, etc. without any user intervention except of course the command at some point from the user to make the coffee. In order to brew the coffee the coffee machine needs sensors to measure the temperature and amount of water as well as actuators to inject the water and get the beans to brew. We could therefore say they are processing units.

Of course the *System on Chip* comes also with some disadvantages as well. There are many constraints real-time, performance, memory, power. So why not use discrete components offering e.g. more performance, memory, etc.? Well the main aspect is the cost as the cost of microcontroller is far lower than the cost of prgeneral purpose processor.

> The constraint can be also the manufacturing process (availability and volume of production).

## Rust in embedded? That does not sound right!
I agree to disagree. It's true that *C/C++* dominates the embedded industry together with small share of assembly and [DSL] languages (here I mean languages for [PLC]s, which are often themselves written in C/C+). Sometimes you can see behemoths like *JAVA* (thanks to [Jazzele] extension), *Python* (not fully fledge python, just [Micropython]) and others.

> I should not forget to mention [Ada] which stresses the importance of code safety.

And there you have your answer. If any of these languages can run in embedded environment than why not *Rust*? It might not have the market share of *C/C++* but it also doesn't have many of it's flaws. But less not be rude and start by the pros of *C/C++* language. After all we are gentlemen (and ladies, of course).

## Overview of C language
I found this [thread] listing the following pros:
* Performance
* Direct access to low level hardware *API's*
* You can find a *C* compiler for the vast majority of these devices
* *C* (the runtime and your generated executable) is "small". You don't have to load a bunch of stuff into the system to get the code running
* The hardware API/driver(s) will likely be written in *C* or *C++*
* *C* was designed to model a CPU (or vice versa in case of *ARM Cortex-M*)

I would add three more points:
* Number of software engineers that know *C* language (more of an issue to a project manager)
* Lot of the embedded materials use *C* language as their default (here I hope will address this issue)
* Lots of legacy code written in *C*

I'm sure there are more pros but these are the most crucial (at least from my perspective). So why should we opt for a change if there are these benefits? Because there are also cons and I'd say from the development and maintenance point of view they become a nightmare - that might be the reason to look for alternatives to *C*.

So what points/[flaws] do we need to address in *C* language?

* Wrong kind of simplicity
> The language limits the programmer and programs written in *C* become a lot more complex and lengthy
* Poor tools for abstraction
> This limitation leads to use of ugly hacks to get around the limited abstraction tools
* Poor tools for safety
> No check for memory leaks (buffer overflows, use-after-free or double-free bugs), crashes or exploitable security holes
* No [RAII]
> Completely lacks support for the RAII idiom, which is an integral part of *C++* and covers also the previous two points
* Poor standard library
> Provides only basic *I/O* and some string manipulation functions
* To many standards and guidelines
> There are multiple standards *ANSIC*, *C99*, lately *C11* and some guidelines as [MISRA C]. These are not part of the language standard and need to be enforced separately

* Package/Library manager  - tests, deployment, dependencies
>   There is no universal package/library manager that solves dependencies in libraries, build of the project, execution of tests, deployment ... Yes there is e.g. [Conan] but again it's not enforced by the language
* Macros
> I'd suggest to minimize the usage as they can be quite nasty
* Documentation
> There is also no universal documentation tool. [Doxygen] comes to mind

## Rust to save the damsel in distress
So we walked over the pros and cons of *C* language. I did not cover *C++* as then this post would turn into a comparison of various languages and spark a very intensive argument (fueled by trolls). Now let's look how Rust can as the title suggest save *the damsel in distress*. But first let's check our hero can pass the bar and compare with the mighty *C* hero.

* Performance
> You can write the same code in Rust as you would in *C* and you can leverage the type system especially when dealing with concurrency code
* Direct access to low level hardware API's
> You can also access the low level hardware API's though direct memory operations require use of unsafe and you as programmer has to ensure the code behaves correctly
* You can find a *C* compiler for the vast majority of these devices
> Rust leverages the mature [LLVM] compiler infrastructure project - there are multiple architectures supported. Also the frontend is written in Rust itself.
* *C* (the runtime and your generated executable) is *"small"*. You don't have to load a bunch of stuff into the system to get the code running
> Rust has almost no run time environment and the code generated by the compiler is small thanks to the optimalization in LLVM (in IR and LTO)
* The hardware API/driver(s) will likely be written in *C* or *C++*
> Rust has no ABI defined and can use C ABI. The API can look the same as it does in C/C++ or we can apply design pattern to provide better abstraction and modularity of the code.
* *C* was designed to model a CPU (or vice versa in case of *ARM Cortex-M*)
> Simillar to last point. Rust also supports inline assembly.

So far *Rust* and *C* are worthy of the quest. Let's have a look how *Rust* addresses the weakness of its oponent.

* Wrong kind of simplicity
> Rust has pretty steep learning curve, but the language seems mature and well designed. New features are also added carefully.
* Poor tools for abstraction
> Rust allows you to touch low level stuff like the hardware itself but at the same time it allows you to create zero cost abstraction in the code
* Poor tools for safety
> Rust has many checks that catch the potential errors during compilation instead (no buffer overflows, dangling pointers nor data races) during run time and enforces programmer to use unsafe keyword when accessing memory directly as the compiler cannot verify the correctness of the outcome of the code (allowing to dereference raw pointers, call unsafe functions, access mutable statics and implement unsafe traits)
* No *RAII*
> Rust uses heavily *RAII* which is implemented by the help of the compiler instead of e.g. reference counting in the run time.
* Poor standard library
> Rust standard library provides many types and functions.
* To many standards and guidelines
> Rust accepts RFCs to add/change/remove features in the Rust specification. Most of the checking is implemented in the compiler itself at various stages though external tools like clippy exist (sort of a lint, give it a try).
* Package/Library manager - tests, deployment, dependencies
> Rust comes with a package manger called Cargo and a hub full of libraries on [crates.io] . There are many plugins for the Cargo package manager - look for cargo- on crates.io
* Macros
> After you reach the summit (you learn to some extend Rust lang) you can continue up the stairs to the heaven as the macro system is also quite complex. However, macros are safe and are used quite often in Rust
* Documentation
> As the documentation is tedious to write and the languages (libraries) change from time to time Rust allows you to generate the documentation directly from your code with the support of Markdown syntax and the ability to add snippets of library usage that can be executed as well as tested

The last advantage that comes to mind is the unified compiler support thanks to the *LLVM*. One could argue about having just one compiler implementation (*rustc*, there is also another implementation called [mrustc]). However, especially in the embedded world I've see how two *C/C++* compilers compile differently the same source code (don't remember the exact snipper, but it concerned templates and declaration of functions) not to mention the parameters passed to them differs greatly.

I'll leave the disadvantage of *Rust* language up to you or [duckduckgo].

Ok, this seems that *Rust* should have no problem to slay the dragon and save the damsel in distress, right? But why do it alone? I have left out one key aspect of Rust that is the key to success. Why don't we work together?
As there is a lot of software written in *C* why don't we reuse this code through *Foreign Function Interface* (FFI) and save us some time and energy? With *Rust* we can do exactly that.

> Although in heroic songs it might sound better to kill a dragon alone in real life it's better to do it in group.

## Back to the metal
So back at the beginning before we submerged into the *C* vs *Rust*, we discussed what the microcontroller is, right? With that explained and armed with a suitable language we need to select the proper microcontroller that will be supported by *LLVM*.

Over the time microcontrollers evolved greatly as the process of miniaturization allowed to get more and more transistors on the die (check [Morse's law]). Back in the 70s we had [4-bit microcontrollers] - meaning the system bus was 4 bits wide. Later we got 8-bit microcontrollers that are today still in use (mainly due to the cost and already developed tools and software) as well as 16-bit microcontrollers.

Yet recently a new (well the company is a veteran in processor design) player with 32-bit microcontrollers has emerged on the market with a good business model. Instead of making the chip directly let's sell the core and optionally also the peripherals to different vendors who finish the microcontroller by adding peripheral and other logic to the purchased core - making a real chip that they sell. The comany behind this approach is *ARM* and their series of *Cortex-M* cores that I'm about to cover.

The [ARM Cortex-M] architecture is very well supported by the *LLVM* toolchain. Also as the cores are licensed they are the same and the code is easily ported from one vendor to the other (most of the time).

So what is the selling point behind these cores?

* Binary compatibility
> Code compiled to one core will be compatible with newer cores
* Support for *C* (in our case *Rust* :)
> The microcontroller can be programmed directly in C
* Support for OS
> The core itself has been designed in mind for operating systems (though not exactly Windows Vista)

And also many other features but I'm not a marketing guy from ARM.

## STM32F4 Discovery

**TODO: Review needed**

So for the actual board we'll use STM32F4 Discovery to which we'll simply refer as "F4" thoughout the posts. Here is a brief summary of the board:

- A *STM32F407VGT6* microcontroller:
  - A single-core ARM Cortex-M4F processor with hardware support for single-precision floating pointoperations and digital signal processing and a maximum clock frequency of 168 MHz.
  - 1024 KiB of "Flash" memory. (1 KiB = 1024 bytes)
  - 192 KiB of RAM.
  - many "peripherals": timers, GPIO, I2C, SPI, USART, etc.
  - lots of "pins" that are exposed in the two lateral "headers".
  - Operational voltage around 3.3V
- A digital [accelerometer](https://en.wikipedia.org/wiki/Accelerometer)
- A digital microphone, an audio DAC with integrated class
  D speaker driver


- 4 user LEDs arranged in the shape of a cross
- A second microcontroller: a STM32F103CBT. This microcontroller is actually part of an on-boardprogrammer and debugger named ST-LINK and is connected to the USB port named "USB ST-LINK".
- There's a second USB port connected to the main microcontroller, the STM32F407VGT6, and can be used in applications.

## Taming the beast
Before we end this post let's look at the necessary steps to take to tame the beast - develop and deploy an application:

* GNU GCC compiler
* GNU Binutils
 * GNU Linker
 * GNU objcopy
 * GNU objdump

> These will be supplemented by rustc

* GNU debugger
* OpenOCD
* [QEMU]

# Summary

Today we only talked about *Rust*, *C/C++* and microcontrollers. But stay tuned for the [next](chapter_2.html) post as we'll start coding our bare metal application. 

> You are of course welcome to go through the chapters in whatever order you want. In case you are interested in particular topic just hit **S** and type the name of the topic in the focused search field.

[email]: **TODO: Add email address**

[SoC]: https://en.wikipedia.org/wiki/System_on_a_chip
[Ada]: https://en.wikipedia.org/wiki/Ada_(programming_language)
[ARM]: https://en.wikipedia.org/wiki/ARM_architecture
[Rust]: https://www.rust-lang.org/en-US/
[DSL]: https://en.wikipedia.org/wiki/Domain-specific_language
[PLC]: https://en.wikipedia.org/wiki/Programmable_logic_controller
[Jazzele]: https://en.wikipedia.org/wiki/Jazelle
[Micropython]: https://micropython.org/
[thread]: https://softwareengineering.stackexchange.com/questions/84514/why-does-c-dominate-in-the-embedded-software-market#84515
[flaws]: http://warp.povusers.org/grrr/HateC.html
[RAII]: https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization
[MISRA C]: https://en.wikipedia.org/wiki/MISRA_C
[Conan]: https://conan.io/
[Doxygen]: http://www.doxygen.nl/
[LLVM]: https://llvm.org/
[Morse's law]: https://en.wikipedia.org/wiki/Moore%27s_law
[4-bit microcontrollers]: https://en.wikipedia.org/wiki/Intel_4004
[ARM Cortex-M]: https://en.wikipedia.org/wiki/ARM_Cortex-M
[IDE]: https://en.wikipedia.org/wiki/Integrated_development_environment
[STM32F4 Discovery kit]: https://www.st.com/en/evaluation-tools/stm32f4discovery.html
[QEMU]: https://www.qemu.org/
[ crates.io ]: http://www,crates.io
[mrustc]: https://github.com/thepowersgang/mrustc
[ duckduckgo ]: http://duckduckgo.com

