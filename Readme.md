# Cortex book

Book of many chapters that will teach you the black magic of running Rust on embedded devices such as ARM Cortex-M.

## Build instructions

To build run:

```
mdbook build
```

## Rendered version

There is a job as a part of Gitlab CI which renders the book as a set of static webpages. 

https://phodina.gitlab.io/cortex-book/