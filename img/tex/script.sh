#!/bin/bash
for filename in $PWD/*; do
	name=$(basename $"filename")
	ext="${filename##*.}"
	if [ ${ext} == "pdf" ]
	then
		echo "Converting $filename"
		echo $PWD/`basename $filename .png`
		magick convert -density 300 -background white -define profile:skip=ICC -alpha remove $filename -quality 100 "$PWD/`basename $filename .png`" 
	fi
done

